#!/usr/bin/env python3
#
# ex:ts=4:sw=4:sts=4:et
# -*- tab-width: 4; c-basic-offset: 4; indent-tabs-mode: nil -*-
#
# Security Response Tool Commandline Tool
#
# Copyright (C) 2018       Wind River Systems
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

### Usage Examples (run from top level directory)
# Init YP products:   ./bin/yp/srtool_yp.py --init-products


import os
import sys
import argparse
import sqlite3
import json

# load the srt.sqlite schema indexes
dir_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.insert(0, dir_path)
from common.srt_schema import ORM

# Setup:
srtDbName = 'srt.sqlite'


#################################
# Helper methods
#

verbose = False

def debugMsg(msg):
    if verbose:
        print(msg)

overrides = {}

def set_override(key,value=None):
    if not value is None:
        overrides[key] = value
    elif key in os.environ.keys():
        overrides[key] = 'yes' if os.environ[key].startswith('1') else 'no'
    else:
        overrides[key] = ''
    if overrides[key]:
        print("OVERRIDE: %s = %s" % (key,overrides[key]))

def get_override(key):
    if key in overrides.keys():
        return overrides[key]
    return ''

#################################
# Initialize the product list
#

def init_products(source_file):

    source_doc = os.path.join(srtool_basepath,source_file)
    with open(source_doc) as json_data:
        dct = json.load(json_data)

    conn = sqlite3.connect(srtDbName)
    cur = conn.cursor()

    Product_Items = dct['Product_Items']
    for i, Product_Item  in enumerate(Product_Items):
        order = Product_Item['order']
        key = Product_Item['key']
        name = Product_Item['name']
        version = Product_Item['version']
        profile = Product_Item['profile']
        cpe = Product_Item['cpe']
        defect_tags = Product_Item['defect_tags']
        product_tags = Product_Item['product_tags']

        sql = "SELECT * FROM orm_product WHERE key = '%s'" % (key, )
        product = cur.execute(sql).fetchone()
        if product is None:
            # NOTE: 'order' is a reserved SQL keyword, so we have to quote it
            sql = ''' INSERT into orm_product ("order", key, name, version, profile, cpe, defect_tags, product_tags) VALUES (?, ?, ?, ?, ?, ?, ?, ?)'''
            cur.execute(sql, (order, key, name, version, profile, cpe, defect_tags, product_tags))
        else:
            sql = ''' UPDATE orm_product
                      SET "order" = ?, key = ?, name = ?, version = ?, profile = ?, cpe= ?, defect_tags=?, product_tags=?
                      WHERE id=?'''
            cur.execute(sql, (order, key, name, version, profile, cpe, defect_tags, product_tags, product[ORM.PRODUCT_ID]))
    conn.commit()

#################################
# main loop
#

def main(argv):
    global verbose

    parser = argparse.ArgumentParser(description='srtool_wr.py: Manage SRTool to Yocto Project')
    parser.add_argument('--init-products', '-p', action='store_const', const='init_products', dest='command', help='Init and import Yocto Project Products')
    parser.add_argument('--file', dest='file', help='Source file')

    parser.add_argument('--force', '-f', action='store_true', dest='force_update', help='Force update')
    parser.add_argument('--verbose', '-v', action='store_true', dest='verbose', help='Verbose debugging')
    args = parser.parse_args()

    master_log = open("./update_logs/master_log.txt", "a")

    verbose = args.verbose

    # required parameter for the following commands
    if not args.file:
        print("ERROR: missing 'file' argument")
        exit(1)

    if 'init_products' == args.command:
        init_products(args.file)
    else:
        print("Command not found")

if __name__ == '__main__':
    srtool_scriptpath = os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))
    srtool_basepath = os.path.dirname(srtool_scriptpath)
    main(sys.argv[1:])
