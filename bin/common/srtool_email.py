#!/usr/bin/env python3
#
# ex:ts=4:sw=4:sts=4:et
# -*- tab-width: 4; c-basic-offset: 4; indent-tabs-mode: nil -*-
#
# Security Response Tool Implementation
#
# Copyright (C) 2017       Wind River Systems
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

#
# Theory of operation
#
#  * Send an email via Python's smtplib
#  * Support encryption and passwords

import os
import sys
import argparse
import smtplib
from email.mime.text import MIMEText

# Setup:
toaddrs = ''
fromaddr = ''
smtpserver = ''
smtpencryption = ''
srt_user = ''
srt_passwd = ''
subject = ''
msg = ''
verbose = False
test = False

srtErrorLog = 'srt_errors.txt'

#################################
# Send the email
#

def prompt(prompt):
    return input(prompt).strip()

def enter_message():
    global msg

    print("Enter message, end with ^D (Unix) or ^Z (Windows):")
    msg = ''
    while True:
        try:
            line = input()
        except EOFError:
            break
        if not line:
            break
        msg = msg + line

    print("Message length is", len(msg))

def srt_error_log(msg):
    f1=open(srtErrorLog, 'a')
    f1.write("|" + msg + "|\n" )
    f1.close()

#################################
# Send the email
#

def send_email():
    global msg

    # Add the headers at the start!
    msg = MIMEText(msg)
    msg['Subject'] = subject
    msg['From'] = fromaddr
    msg['To'] = ', '.join(toaddrs)

    if test:
        print("From:%s" % fromaddr)
        print("To:%s" % toaddrs)
        print("Subject:%s" % subject)
        if 'tls' == smtpencryption:
            print("TLS:yes")
        if srt_user:
            print("User:%s" % srt_user)
        print("Message:")
        print("----" % msg)
        print("%s" % msg.as_string())
        print("----" % msg)
    else:
        server = smtplib.SMTP(smtpserver)
        if verbose:
            server.set_debuglevel(1)
        if 'tls' == smtpencryption:
            server.starttls()
        if srt_user:
            server.login(srt_user, srt_passwd)
        server.sendmail(fromaddr, toaddrs, msg.as_string())
        server.quit()

#################################
# main loop
#

def main(argv):
    global toaddrs
    global fromaddr
    global smtpserver
    global smtpencryption
    global srt_user
    global srt_passwd
    global subject
    global msg
    global verbose
    global test

    parser = argparse.ArgumentParser(description='srtool_email.py: SRTool email handler')
    parser.add_argument('--from', dest='smtpfrom', help='From email address')
    parser.add_argument('--to', dest='smtpto', help='To email address')
    parser.add_argument('--subject', '-s', dest='subject', help='Subject for email address')
    parser.add_argument('--server', dest='smtpserver', help='SMTP server address')
    parser.add_argument('--user', dest='user', help='User name for Jira access')
    parser.add_argument('--passwd', dest='passwd', help='User password for Jira access')
    parser.add_argument('--tls', '-t', action='store_true', dest='tls', help='Use TLS encryption')
    parser.add_argument('--message', '-m', dest='message', help='Message to send')
    parser.add_argument('--file', '-f', dest='file', help='File to send')
    parser.add_argument('--prompt', '-p', action='store_true', dest='prompt', help='Directly enter a message')
    parser.add_argument('--verbose', '-v', action='store_true', dest='verbose', help='Verbose output from SMTP connection')
    parser.add_argument('--test', action='store_true', dest='test', help='Test the email setup')
    args = parser.parse_args()

    # Resolve the arguments
    if args.smtpto:
        toaddrs = args.smtpto
        toaddrs = toaddrs.split(',')
    else:
        print("ERROR: missing 'to' address", file=sys.stderr)
        exit(1)
    if args.subject:
        subject = args.subject
    else:
        print("ERROR: missing 'subject'", file=sys.stderr)
        exit(1)
    verbose = args.verbose
    test = args.test

    # Authorization
    if args.user:
        srt_user = args.user
    else:
        srt_user = os.getenv('SRT_EMAIL_USER','')
    if args.passwd:
        srt_passwd = args.passwd
    else:
        srt_passwd = os.getenv('SRT_EMAIL_PASSWD','')
    # Server settings
    if args.smtpserver:
        smtpserver = args.smtpserver
    else:
        smtpserver = os.getenv('SRT_EMAIL_SMTP','')
    if args.smtpfrom:
        fromaddr = args.smtpfrom
    else:
        fromaddr = os.getenv('SRT_EMAIL_FROM','')
    if not smtpserver or not srt_user or not srt_passwd or not fromaddr:
        msg = "FATAL ERROR: Missing SMTP('%s'), User('%s'), Password('%s'), and/or From('%s') for email access" % (smtpserver,srt_user,'*******' if srt_passwd else '',fromaddr)
        print(msg, file=sys.stderr)
        srt_error_log(msg)
        exit(1)

    # Encryption
    if args.tls:
        smtpencryption = 'tls'
    else:
        smtpencryption = ''

    # Set up the message
    if args.message:
        msg = args.message
    elif args.file:
        fp = open(args.file, 'rb')
        # Create a text/plain message
        msg = fp.read()
        fp.close()
    elif args.prompt:
        enter_message()
    else:
        print("ERROR: missing message source", file=sys.stderr)
        exit(1)

    # Debug
    if verbose:
        srt_error_log("EMAIL: SMTP('%s'), User('%s'), Password('%s'), From('%s'), args('%s')" % (smtpserver,srt_user,srt_passwd,fromaddr,str(args)))

    # Send the email
    send_email()


if __name__ == '__main__':
    script_pathname=os.path.dirname(sys.argv[0])
    main(sys.argv[1:])

