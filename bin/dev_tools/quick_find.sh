#!/bin/bash

# SRTool helper script to quickly find strings in the source

find bin -exec grep -l "$1" {} \; 2> /dev/null
find lib -exec grep -l "$1" {} \; 2> /dev/null

