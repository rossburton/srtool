#!/bin/bash

# This must run as a sourced script
if [ "$0" = "$BASH_SOURCE" ]; then
    echo "Error: Script must be sourced"
    exit 1
fi

mode="$1"
echo "mode=|$mode|"

if [ "debug" != "$mode" ] ; then
    # Standard Environment
	echo "=== SRTool STANDARD  MODE ==="
    export SRT_PORT=9000
    # Quick development log
    export SRTDBG_LOG=`pwd`/srt_dbg.log
    # Development/debugging flags
    export SRTDBG_MINIMAL_DB=0
    export SRTDBG_SKIP_DEFECT_IMPORT=0
    export SRTDBG_SKIP_CVE_IMPORT=0
    export SRTDBG_SKIP_CPE_IMPORT=0
    export SRT_SKIP_AUTOUPDATE=0
    # Email credentials
    export SRT_EMAIL_SMTP=smtp.org.com
    export SRT_EMAIL_PASSWD=temp_password
    export SRT_EMAIL_USER=temp_user
    export SRT_EMAIL_FROM=temp_user@org.com
    # Defect (e.g. Jira) credentials
    export SRT_DEFECT_PASSWD=temp_password
    export SRT_DEFECT_USER=temp_user
else
    # Minimal Development Environment
    # Standard Environment
	echo "=== SRTool DEBUG MODE ==="
    export SRT_PORT=9990
    # Quick development log
    export SRTDBG_LOG=`pwd`/srt_dbg.log
    # Development/debugging flags
    export SRTDBG_MINIMAL_DB=1
    export SRTDBG_SKIP_DEFECT_IMPORT=1
    export SRTDBG_SKIP_CVE_IMPORT=0
    export SRTDBG_SKIP_CPE_IMPORT=0
    export SRT_SKIP_AUTOUPDATE=1
    # Email credentials
    export SRT_EMAIL_SMTP=smtp.org.com
    export SRT_EMAIL_PASSWD=temp_password
    export SRT_EMAIL_USER=temp_user
    export SRT_EMAIL_FROM=temp_user@org.com
    # Defect (e.g. Jira) credentials
    export SRT_DEFECT_PASSWD=temp_password
    export SRT_DEFECT_USER=temp_user
fi

echo "SRT_PORT=$SRT_PORT ; Change it with: export SRT_PORT=9123"

