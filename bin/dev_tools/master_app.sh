#!/bin/bash

#
# Set the master app by:
#  (a) renaming the other master(s) datasource files
#  (b) enabling the master datasource file
#
# Create the new master app by:
#  (a) clone the ACME master bin/lib directories
#  (b) replace ACME labels in those files and file names
#  (c) set as the new master app
#

#####################
# common routines
#

srtErrorLog='srt_errors.txt'

srt_error_log()
{
	echo "|$1|" >> $srtErrorLog
}

#####################
# create_master
#

# Clone and prepare the new app from the ACME sample app
create_master ()
{
	if [ -z "$master_app" ] ; then
		echo "ERROR: missing new master app name"
		return 1
	fi
	if [ -d "bin/$master_app" ] ; then
		echo "ERROR: new master app directory already exists:<$master_app>"
		return 1
	fi

	# Refactor app names
	title_name="$(tr '[:lower:]' '[:upper:]' <<< ${master_app:0:1})${master_app:1}"
	upper_name="$(tr '[:lower:]' '[:upper:]' <<< ${master_app})"
	lower_name="$(tr '[:upper:]' '[:lower:]' <<< ${master_app})"

	# Trial create new master dir, also tests name format
	mkdir -p bin/$lower_name
	mkdir -p lib/$lower_name
	if [ 0 -ne $? ] ; then
		echo "ERROR: problem creating new master app directories:<*/$lower_name>"
		exit 1
	fi

	# Copy over 'acme' directories
	cp -r bin/acme/* bin/$lower_name
	cp -r lib/acme/* lib/$lower_name

	# Refactor the files
	find bin/$lower_name -exec $0 refactor $title_name $upper_name $lower_name {} \; 2> /dev/null
	find lib/$lower_name -exec $0 refactor $title_name $upper_name $lower_name {} \; 2> /dev/null

	echo "=== New master app <$master_app> created! ==="
	echo "  bin/$master_app"
	echo "  lib/$master_app"
}

# Refactor the app labels within the files and file names
refactor ()
{
	title_name=$2
	upper_name=$3
	lower_name=$4
	file_acme=$5

	# Leave the ACME png files alone as placeholders - they need to be manually re-created
	if [ "$file_acme" != "${file_acme/\.png//}" ] ; then
		return
	fi

	# Rename the file
	file_name="$(echo $file_acme | sed -e "s|acme|$lower_name|")"
	if [ "$file_acme" != "$file_name" ] ; then
		mv -f $file_acme $file_name
	fi

	# Do not bother to refactor cache files
	if [ "$file_acme" != "${file_acme/__pycache__//}" ] ; then
		return
	fi

	# Refactor the file
	sed -i -e "s|Acme|$title_name|g" $file_name
	sed -i -e "s|ACME|$upper_name|g" $file_name
	sed -i -e "s|acme|$lower_name|g" $file_name
}

#####################
# main()
#

master_app=$1

# Creation support
if [ "create" = "$master_app" ] ; then
	master_app=$2
	if [ 'yes' != $(echo "$master_app" | sed -e "s|^[a-z][a-z0-9]*$|yes|") ] ; then
		echo "ERROR: master app name <$master_app> must start with letter and then only have letters and numbers, and be in lower case"
		exit 1
	fi
	echo "$(create_master $master_app)"
	# continue to set as new master app ...
fi
if [ "refactor" = "$master_app" ] ; then
	refactor $*
	exit 0
fi

# Set master
if [ -z "$master_app" ] ; then
    master_app="yp"
fi
if [ ! -d "./bin/$master_app" ] ; then
    echo "ERROR: master app directory missing"
    exit 1
fi

FS=$'\n'       # make newlines the only separator
for p in $(find bin -name srtool_env.sh -exec grep -l "SRT_MAIN_APP" {} \;) ; do
    if [ "${p}" != "${p/bin\/yp/}" ] ; then
        # Always leave the 'yp' directory alone
        continue
    fi
    echo "Check:$p"
    ds_dir=$(dirname $p)
    if [ "${p}" != "${p/bin\/$master_app/}" ] ; then
        echo "SET_MASTER:$p"
        mv -f $ds_dir/datasource.json_sample $ds_dir/datasource.json 2> /dev/null
    else
        echo "DISABLE_MASTER:$p"
        mv -f $ds_dir/datasource.json $ds_dir/datasource.json_sample 2> /dev/null
        # Remove old app's datasources
        prev_app=$(basename $ds_dir)
        bin/common/srtool_utils.py --remove-app-sources $prev_app
    fi
done

if [ "yp" = "$master_app" ] ; then
    echo "SET_MASTER:./bin/$master_app"
else
    echo bin/common/srtool_utils.py --remove-app-sources yp
fi
