#!/bin/bash

# SRTool helper script to redo the instance from scratch
# For example to test schema or processing changes

./stop.sh
rm ./srt.sqlite 2> /dev/null
#git pull
#git clean -f -d
./start.sh

