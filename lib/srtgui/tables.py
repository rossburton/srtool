#
# ex:ts=4:sw=4:sts=4:et
# -*- tab-width: 4; c-basic-offset: 4; indent-tabs-mode: nil -*-
#
# Security Response Tool Implementation
#
# Copyright (C) 2017       Wind River Systems
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from srtgui.widgets import ToasterTable
from orm.models import Cve, Vulnerability, Investigation, CweTable, Product
from orm.models import Package
from orm.models import CpeTable, CpeFilter, Defect, DataSource
from orm.models import PublishPending
from orm.models import Notify, NotifyCategories
from users.models import UserSafe

from django.db.models import Q

from srtgui.tablefilter import TableFilter
from srtgui.tablefilter import TableFilterActionToggle

import re

# quick development/debugging support
from srtgui.api import _log

class CvesTable(ToasterTable):
    """Table of All CVE's in SRTool"""

    def __init__(self, *args, **kwargs):
        super(CvesTable, self).__init__(*args, **kwargs)
        self.default_orderby = "name_sort"

    def get_context_data(self, **kwargs):
        context = super(CvesTable, self).get_context_data(**kwargs)
        return context

    def setup_filters(self, *args, **kwargs):
        # Is Status filter
        is_status = TableFilter(name="is_status",
                                       title="Filter CVE's by 'Status'")
        for status in range(len(Cve.STATUS)):
            is_status.add_action(TableFilterActionToggle(
                Cve.STATUS[status][1].lower().replace(' ','_'),
                Cve.STATUS[status][1],
                Q(status=Cve.STATUS[status][0]))
                )
        self.add_filter(is_status)

        # Recommends filter
        is_recommend = TableFilter(name="is_recommend",
                                       title="Filter CVE's by 'Score'")
        exec_m3 = TableFilterActionToggle(
            "m3",
            "Score = -3",
            Q(recommend=-3))
        exec_m2 = TableFilterActionToggle(
            "m2",
            "Score = -2",
            Q(recommend=-2))
        exec_m1 = TableFilterActionToggle(
            "m1",
            "Score = -1",
            Q(recommend=-1))
        exec_p0 = TableFilterActionToggle(
            "p0",
            "Score = 0",
            Q(recommend=0))
        exec_p1 = TableFilterActionToggle(
            "p1",
            "Score = 1",
            Q(recommend=1))
        exec_p2 = TableFilterActionToggle(
            "p2",
            "Score = 2",
            Q(recommend=2))
        exec_p3 = TableFilterActionToggle(
            "p3",
            "Score = 3",
            Q(recommend=3))
        is_recommend.add_action(exec_m3)
        is_recommend.add_action(exec_m2)
        is_recommend.add_action(exec_m1)
        is_recommend.add_action(exec_p0)
        is_recommend.add_action(exec_p1)
        is_recommend.add_action(exec_p2)
        is_recommend.add_action(exec_p3)
        self.add_filter(is_recommend)

    def setup_queryset(self, *args, **kwargs):
        self.queryset = \
                Cve.objects.all()

        # filter out hidden records
        if not UserSafe.is_admin(self.request.user):
            self.queryset = self.queryset.exclude(public = False)

        self.queryset = self.queryset.order_by(self.default_orderby)


    def setup_columns(self, *args, **kwargs):

        id_link_template = '''
            <a href="{% url 'cve' data.name %}" id="dataid_{{data.id}}">{{data.name}}</a>
        '''
        self.add_column(title="Name",
                        hideable=False,
                        orderable=True,
                        field_name="name_sort",
                        static_data_name="name_sort",
                        static_data_template=id_link_template,
                        )

        self.add_column(title="Name Sort",
                        hideable=True,
                        hidden=True,
                        static_data_name="real_name_sort",
                        static_data_template="{{data.name_sort}}"
                        )

        self.add_column(title="Status",
                        field_name="status",
                        hideable=True,
                        orderable=True,
                        filter_name="is_status",
                        static_data_name="status",
                        static_data_template="{{data.get_status_text}}"
                        )

        score_link_template = '''
            {% if 0 == data.recommend %}0{% else %}{{data.recommend}}{% endif %}
        '''
        self.add_column(title="Score",
                        field_name="recommend",
                        orderable=True,
                        hideable=True,
                        hidden=True,
                        filter_name="is_recommend",
                        static_data_name="recommend",
                        static_data_template=score_link_template,
                        )

        self.add_column(title="Recomends",
                        field_name="recommend_list",
                        hideable=True,
                        hidden=True,
                        )

        self.add_column(title="Data Type",
                        field_name="cve_data_type",
                        hideable=True,
                        hidden=True,
                        )

        self.add_column(title="Data Format",
                        field_name="cve_data_format",
                        hideable=True,
                        hidden=True,
                        )

        self.add_column(title="Data Version",
                        field_name="cve_data_version",
                        hideable=True,
                        hidden=True,
                        )

        self.add_column(title="Description",
                        field_name="description",
                        hideable=False,
                        )

        priority_v3_template = '''
            {{data.cvssV3_baseScore}} {{data.cvssV3_baseSeverity}}
        '''
        self.add_column(title="Priority (V3)",
                        help_text="Priority of the CVE (v3)",
                        hideable=False,
                        orderable=False,
                        static_data_name='priority_v3',
                        static_data_template=priority_v3_template,
                        )

        priority_v2_template = '''
            {{data.cvssV2_baseScore}} {{data.cvssV2_severity}}
        '''
        self.add_column(title="Priority (V2)",
                        help_text="Priority of the CVE (v2)",
                        hideable=True,
                        hidden=True,
                        orderable=False,
                        static_data_name='priority_v2',
                        static_data_template=priority_v2_template,
                        )

        self.add_column(title="Packages",
                        field_name="packages",
                        hideable=True,
                        hidden=True,
                        )

        self.add_column(title="Published",
                        help_text="Initial publish date of the CVE",
                        hideable=False,
                        #orderable=True,
                        field_name="publishedDate",
                        )

        self.add_column(title="Modified",
                        help_text="Last modification date of the CVE",
                        hideable=True,
                        #orderable=True,
                        field_name="lastModifiedDate",
                        )

        self.add_column(title="Comments",
                        field_name="comments",
                        hideable=True,
                        orderable=True,
                        )

        if UserSafe.is_admin(self.request.user):
            self.add_column(title="Comments Private",
                            field_name="comments_private",
                            hideable=True,
                            )

        self.add_column(title="Publish Request",
                        help_text="SRT Publish Request State",
                        hideable=True,
                        hidden=True,
                        orderable=True,
                        field_name="publish_state",
                        static_data_name="publish_state",
                        static_data_template='{{data.get_publish_text}}',
                        )

        self.add_column(title="Publish Date",
                        help_text="SRT Publish date of the CVE",
                        hideable=True,
                        hidden=True,
                        orderable=True,
                        field_name="publish_date"
                        )

        vulnerability_link_template = '''
            {% for cv in data.cve_to_vulnerability.all %}
              {% if not forloop.first %} {% endif %}<a href="{% url 'vulnerability' cv.vulnerability.name %}">{{cv.vulnerability.name}}</a>
            {% endfor %}
        '''
        self.add_column(title="Vulnerability",
                        static_data_name="vulnerability",
                        static_data_template=vulnerability_link_template,
#                        static_data_template='',
                        hidden=False,
                        )

        defect_link_template = '''
            {% for cv in data.cve_to_vulnerability.all %}
              {% for investigation in cv.vulnerability.vulnerability_investigation.all %}
                {% for id in investigation.investigation_to_defect.all %}
                  {% if not forloop.first %} {% endif %}<a href="{% url 'defect_name' id.defect.name %}">{{id.defect.name}}</a>
                {% endfor %}
              {% endfor %}
            {% endfor %}
        '''
        self.add_column(title="Defect",
                        static_data_name="defect",
                        static_data_template=defect_link_template,
#                        static_data_template='',
                        hidden=False,
                        )

        self.add_column(title="SRT Update",
                        hideable=True,
                        hidden=True,
                        orderable=True,
                        field_name="srt_updated",
                        static_data_name="srt_updated",
                        static_data_template='{{data.srt_updated | date:"m/d/y H:i"}}'
                        )

        source_count_template = '''
            {{data.cve_parent.all.count}}
        '''
        self.add_column(title="Source Count",
                        help_text="Number of data sources",
                        hideable=False,
                        orderable=False,
                        static_data_name='source_count',
                        static_data_template=source_count_template,
                        )


class SelectCveTable(ToasterTable):
    """Table of Selectable CVE's in SRTool"""

    def __init__(self, *args, **kwargs):
        super(SelectCveTable, self).__init__(*args, **kwargs)
        self.default_orderby = "name_sort"

    def get_context_data(self,**kwargs):
        context = super(SelectCveTable, self).get_context_data(**kwargs)
        context['products'] = Product.objects.all()
        context['components'] = Defect.Components
        return context

    def apply_row_customization(self, row):
        data = super(SelectCveTable, self).apply_row_customization(row)
        # data:dict_keys(['rows', 'total', 'default_orderby', 'error', 'columns'])

        # comments_private -> recommend_list
        for i in range(len(data['rows'])):
            data['rows'][i]['for'] = ''
            data['rows'][i]['against'] = ''
            for key in data['rows'][i]['recommend_list'].split(','):
                if key.startswith('+'):
                    data['rows'][i]['for'] += '%s ' % key[1:]
                elif key.startswith('-'):
                    data['rows'][i]['against'] += '%s ' % key[1:]
            # Add toggle tags
            try:
                box_id = re.findall(r'box_\d+', data['rows'][i]['select'])[0]
                data['rows'][i]['for']     = '<span onclick="toggle_select(\'%s\');">%s</span>' % (box_id,data['rows'][i]['for'])
                data['rows'][i]['against'] = '<span onclick="toggle_select(\'%s\');">%s</span>' % (box_id,data['rows'][i]['against'])
                data['rows'][i]['recommend_list'] = '<span onclick="toggle_select(\'%s\');">%s</span>' % (box_id,data['rows'][i]['recommend_list'])
            except Exception as e:
                pass

        return data

    def setup_filters(self, *args, **kwargs):
       # Status filter
        is_status = TableFilter(name="is_status",
                                       title="Filter CPE's by Status")
        for status in range(len(Cve.STATUS)):
            _log("FILTER:%s,%s,%s." % (Cve.STATUS[status][1].lower().replace(' ','_'),Cve.STATUS[status][1],Cve.STATUS[status][0]))

            is_status.add_action(TableFilterActionToggle(
                Cve.STATUS[status][1].lower().replace(' ','_'),
                Cve.STATUS[status][1],
                Q(status=Cve.STATUS[status][0]))
                )
        # Append the composite "all new" filter
#        is_status.add_action(TableFilterActionToggle(
#            "all_new",
#            "All New",
#            Q(status=Cve.NEW) & Q(status=Cve.NEW_RESERVED))
#            )
        self.add_filter(is_status)


        # Recommends filter
        is_recommend = TableFilter(name="is_recommend",
                                       title="Filter CVE's by 'Score")
        exec_m3 = TableFilterActionToggle(
            "m3",
            "Score : <= -3",
            Q(recommend=-3))
        exec_m2 = TableFilterActionToggle(
            "m2",
            "Score : -2",
            Q(recommend=-2))
        exec_m1 = TableFilterActionToggle(
            "m1",
            "Score : -1",
            Q(recommend=-1))
        exec_p0 = TableFilterActionToggle(
            "p0",
            "Score : 0",
            Q(recommend=0))
        exec_p1 = TableFilterActionToggle(
            "p1",
            "Score : 1",
            Q(recommend=1))
        exec_p2 = TableFilterActionToggle(
            "p2",
            "Score : 2",
            Q(recommend=2))
        exec_p3 = TableFilterActionToggle(
            "p3",
            "Score : >= 3",
            Q(recommend=3))
        is_recommend.add_action(exec_m3)
        is_recommend.add_action(exec_m2)
        is_recommend.add_action(exec_m1)
        is_recommend.add_action(exec_p0)
        is_recommend.add_action(exec_p1)
        is_recommend.add_action(exec_p2)
        is_recommend.add_action(exec_p3)
        self.add_filter(is_recommend)


    def setup_queryset(self, *args, **kwargs):
        cve_select_status = self.request.GET.get('cve_status','')
        _log("SETUP_QUERYSET:GET_CONTEXT_DATA:%s" % (cve_select_status))
        if '' != cve_select_status:
            self.queryset = Cve.objects.filter(status = int(cve_select_status))
        else:
            self.queryset = Cve.objects.filter(status = Cve.NEW)

        # filter out hidden records !!! ALL NEW ONES SHOULD BE PUBLIC
#        userAccess = Access(self.request.session.get('srt_user_id', '0'))
#        if not userAccess.is_admin():
#            self.queryset = self.queryset.exclude(public = False)

        self.queryset = self.queryset.order_by(self.default_orderby)


    def setup_columns(self, *args, **kwargs):

        self.add_column(title="Select",
                        field_name="Select",
                        hideable=False,
                        static_data_name="select",
                        static_data_template='<input type="checkbox" id="box_{{data.id}}" name="{{data.name}}" />',
                        )

        self.add_column(title="Status",
                        field_name="status",
                        hideable=False,
                        orderable=True,
                        filter_name="is_status",
                        static_data_name="status",
                        static_data_template='<span onclick="toggle_select(\'box_{{data.id}}\');">{{data.get_status_text}}</span>',
                        )

        # Toggle span added in apply_row_customization()
        self.add_column(title="Recommend List",
                        field_name="recommend_list",
                        hideable=True,
                        hidden=True,
                        static_data_name="recommend_list",
                        static_data_template='{{data.recommend_list}}',
                        )

        recommend_link_template = '''
            {% load projecttags %}<span onclick="toggle_select(\'box_{{data.id}}\');">{{data.recommend|recommend_display}}</span>
        '''
        self.add_column(title="Recommendation",
                        hideable=False,
                        orderable=True,
                        filter_name="is_recommend",
                        field_name="recommend",
                        static_data_name="recommend",
                        static_data_template=recommend_link_template,
                        )

        id_link_template = '''
            <a href="{% url 'cve' data.name %}" id="dataid_{{data.id}}" target="_blank">{{data.name}}</a>
        '''
        self.add_column(title="Name",
                        hideable=False,
                        orderable=True,
                        field_name="name_sort",
                        static_data_name="name_sort",
                        static_data_template=id_link_template,
                        )

        self.add_column(title="Description",
                        field_name="description",
                        hideable=False,
                        static_data_name="description",
                        static_data_template='<span onclick="toggle_select(\'box_{{data.id}}\');">{{data.description}}</span>',
                        )

        priority_v3_template = '''
            <span onclick="toggle_select(\'box_{{data.id}}\');">{{data.cvssV3_baseScore}} {{data.cvssV3_baseSeverity}}</span>
        '''
        self.add_column(title="Priority (V3)",
                        help_text="Priority of the CVE (v3)",
                        hideable=False,
                        orderable=False,
                        static_data_name='priority_v3',
                        static_data_template=priority_v3_template,
                        )

        # Dynamic values in apply_row_customization()
        x_priority_v2_template = '''
            <span onclick="toggle_select(\'box_{{data.id}}\');">{{data.cvssV2_baseScore}} {{data.cvssV2_priority}}</span>
        '''
        self.add_column(title="Priority (V2)",
                        help_text="Priority of the CVE (v2)",
                        hideable=True,
                        hidden=True,
                        orderable=False,
                        static_data_name='priority_v2',
                        static_data_template='',
                        )

        self.add_column(title="publishedDate",
                        field_name="publisheddate",
                        hideable=True,
                        hidden=True,
                        orderable=True,
                        static_data_name="publisheddate",
                        static_data_template='<span onclick="toggle_select(\'box_{{data.id}}\');">{{data.publishedDate}}</span>',
                        )

        self.add_column(title="lastModifiedDate",
                        field_name="lastmodifieddate",
                        hideable=True,
                        hidden=True,
                        orderable=True,
                        static_data_name="lastmodifieddate",
                        static_data_template='<span onclick="toggle_select(\'box_{{data.id}}\');">{{data.lastModifiedDate}}</span>',
                        )


        # Dynamic template values are inserted by apply_row_customization()
        self.add_column(title="Reasons For",
                        help_text="Keywords for accepting this CVE",
                        hideable=True,
                        orderable=False,
                        static_data_name='for',
                        static_data_template='',
                        )

        # Dynamic template values are inserted by apply_row_customization()
        self.add_column(title="Reasons Against",
                        help_text="Keywords for not accepting this CVE",
                        hideable=True,
                        orderable=False,
                        static_data_name='against',
                        static_data_template='',
                        )

        source_count_template = '''
            <span onclick="toggle_select(\'box_{{data.id}}\');">{{data.cve_parent.all.count}}</span>
        '''
        self.add_column(title="Source Count",
                        help_text="Number of data sources",
                        hideable=False,
                        orderable=False,
                        static_data_name='source_count',
                        static_data_template=source_count_template,
                        )


class DefectsTable(ToasterTable):
    """Table of All Defects in SRTool"""

    def __init__(self, *args, **kwargs):
        super(DefectsTable, self).__init__(*args, **kwargs)
        self.default_orderby = "name"

    def get_context_data(self, **kwargs):
        context = super(DefectsTable, self).get_context_data(**kwargs)
        return context


    def setup_filters(self, *args, **kwargs):
        # Priority filter
        is_priority = TableFilter(name="is_priority",
                                       title="Filter defects by 'Priority'")
        for priority in range(len(Defect.Priority)):
            is_priority.add_action(TableFilterActionToggle(
                Defect.Priority[priority][1].lower().replace(' ','_'),
                Defect.Priority[priority][1],
                Q(priority=Defect.Priority[priority][0]))
                )
        self.add_filter(is_priority)

        # Status filter
        is_status = TableFilter(name="is_status",
                                       title="Filter defects by 'Status'")
        for status in range(len(Defect.Status)):
            is_status.add_action(TableFilterActionToggle(
                Defect.Status[status][1].lower().replace(' ','_'),
                Defect.Status[status][1],
                Q(status=Defect.Status[status][0]))
                )
        self.add_filter(is_status)

        # Resolution filter
        is_resolution = TableFilter(name="is_resolution",
                                       title="Filter defects by 'Resolution'")
        for resolution in range(len(Defect.Resolution)):
            is_resolution.add_action(TableFilterActionToggle(
                Defect.Resolution[resolution][1].lower().replace(' ','_'),
                Defect.Resolution[resolution][1],
                Q(resolution=Defect.Resolution[resolution][0]))
                )
        self.add_filter(is_resolution)

        # Product filter
        #(name="Wind River Linux",version="LTS-17")
        is_product = TableFilter(name="is_product",
                                       title="Filter defects by 'Product'")
        for product in Product.objects.all():
            is_product.add_action( TableFilterActionToggle(
                product.key,
                product.long_name,
                Q(product=product))
                )
        self.add_filter(is_product)


    def setup_queryset(self, *args, **kwargs):
        self.queryset = \
                Defect.objects.all()

        self.queryset = self.queryset.order_by(self.default_orderby)

    def setup_columns(self, *args, **kwargs):

        name_link_template = '''
            <a href="{% url 'defect_name' data.name %}" id="dataid_{{data.id}}">{{data.name}}</a>
        '''
        self.add_column(title="Name",
                        hideable=False,
                        orderable=True,
                        field_name="name",
                        static_data_name="name",
                        static_data_template=name_link_template,
                        )

        self.add_column(title="Summary",
                        field_name="summary",
                        )

        self.add_column(title="Priority",
                        hideable=False,
                        field_name="priority",
                        orderable=True,
                        filter_name="is_priority",
                        static_data_name="priority",
                        static_data_template='{{data.get_priority_text}}',
                        )

        self.add_column(title="Status",
                        hideable=False,
                        field_name="status",
                        orderable=True,
                        filter_name="is_status",
                        static_data_name="status",
                        static_data_template='{{data.get_status_text}}',
                        )

        self.add_column(title="Resolution",
                        hideable=False,
                        field_name="resolution",
                        orderable=True,
                        filter_name="is_resolution",
                        static_data_name="resolution",
                        static_data_template='{{data.get_resolution_text}}',
                        )

        self.add_column(title="Release Version",
                        hideable=True,
                        orderable=True,
                        field_name="release_version",
                        )

        self.add_column(title="Publish",
                        hideable=True,
                        orderable=True,
                        field_name="publish",
                        )

        url_link_template = '''
            <a href="{{data.url}}" target="_blank">{{data.url}}</a>
        '''
        self.add_column(title="URL",
                        field_name="url",
                        hideable=True,
                        hidden=True,
                        static_data_name="url",
                        static_data_template=url_link_template,
                        )

#date_created = models.DateField(null=True, blank=True)
#date_updated = models.DateField(null=True, blank=True)

        investigations_link_template = '''
            {% for di in data.defect_to_investigation.all %}
              {% if not forloop.first %} {% endif %}<a href="{% url 'investigation' di.investigation.name %}" target="_blank">{{di.investigation.name}} </a>
            {% endfor %}
        '''
        self.add_column(title="Investigation",
                        hideable=True,
#                        orderable=True,    # multiple investigations
                        static_data_name="investigation",
                        static_data_template=investigations_link_template,
                        )

        # !!! HACK: 'data.product' is returning '%s' when it is supposed to be null !!!
        product_link_template = '''
            {% if data.product != '%s' %}
              <a href="{% url 'product' data.product.id %}">
                {{data.product.long_name}}
              </a>
            {% endif %}
        '''
        self.add_column(title="Product",
                        hideable=True,
                        orderable=True,
                        filter_name="is_product",
                        static_data_name="product",
                        static_data_template=product_link_template,
                        )

        self.add_column(title="SRT Update",
                        hideable=True,
                        hidden=True,
                        orderable=True,
                        field_name="srt_updated",
                        static_data_name="srt_updated",
                        static_data_template='{{data.srt_updated | date:"m/d/y H:i"}}'
                        )

class CwesTable(ToasterTable):
    """Table of All CWE's in SRTool"""

    def __init__(self, *args, **kwargs):
        super(CwesTable, self).__init__(*args, **kwargs)
        self.default_orderby = "name_sort"

    def get_context_data(self, **kwargs):
        context = super(CwesTable, self).get_context_data(**kwargs)
        return context


    def setup_filters(self, *args, **kwargs):
        # is CVE filter
        is_cve = TableFilter(name="is_cve",
                                       title="Filter CWE's by 'CVE'")
        exec_is_cve = TableFilterActionToggle(
            "vulnerable_count",
            "Vulnerable CVE's",
            Q(found=True))
        is_cve.add_action(exec_is_cve)
        self.add_filter(is_cve)


    def setup_queryset(self, *args, **kwargs):
        self.queryset = \
                CweTable.objects.all()

        self.queryset = self.queryset.order_by(self.default_orderby)

    def setup_columns(self, *args, **kwargs):

        self.add_column(title="Name",
                        field_name="name",
                        hideable=False,
                        orderable=True,
                        )

        href_link_template = '''
            <a href="{{data.href}}" id="dataid_{{data.id}} target="_blank">{{data.href}}</a>
        '''
        self.add_column(title="Link",
                        hideable=False,
                        orderable=False,
                        field_name="href",
                        static_data_name="href",
                        static_data_template=href_link_template,
                        )

        self.add_column(title="Summary",
                        field_name="summary",
                        hideable=True,
                        )

        self.add_column(title="Description",
                        field_name="description",
                        hideable=True,
                        )

        self.add_column(title="CVE Count",
                        field_name="vulnerable_count",
                        static_data_name="cves",
                        static_data_template='''<a href="{% url 'cwe_cves'%}?cwe_name={{data.name}}">{{data.vulnerable_count}}</a>''',
                        )

#
# IMPLEMENATION NOTES (neccessary hacks):
# 1) The CVE name is passed by URL attribute because there is
#    there is no obvious way to pass a url() parameter into the ToasterTable
#    (only the explicit 'template_name' appears)
# 2) The CWE record is fetched both in 'get_context_data' and 'setup_queryset'
#    because for some reason the calling sequence is:
#       CweCvesTable.__init__()
#       CweCvesTable.get_context_data()
#       CweCvesTable.__init__()
#       CweCvesTable.setup_queryset()
#    The second init removes any persistence from 'context' to 'query', and 'init'
#    does not have the 'request' attibute assigned at that time.
#
class CweCvesTable(CvesTable):
    def __init__(self, *args, **kwargs):
        super(CweCvesTable, self).__init__(*args, **kwargs)
        self.default_orderby = "name"
        self.cwe = None

    def get_context_data(self, **kwargs):
        cwe_name = self.request.GET.get('cwe_name','')
        self.cwe = CweTable.objects.get(name=cwe_name)

        context = super(CweCvesTable, self).get_context_data(**kwargs)
        context['cwe_name'] = self.cwe.name
        context['cwe_description'] = self.cwe.description
        return context

    def setup_queryset(self, *args, **kwargs):
        cwe_name = self.request.GET.get('cwe_name','')
        self.cwe = CweTable.objects.get(name=cwe_name)

        queryset = self.cwe.cwe2cve.all()
        custom_list = [rec.cve.id for rec in queryset]
        self.queryset = Cve.objects.filter(id__in=custom_list)

        # filter out hidden records
        if not UserSafe.is_admin(self.request.user):
            self.queryset = self.queryset.exclude(public = False)

        self.queryset = self.queryset.order_by(self.default_orderby)


class CpesTable(ToasterTable):
    """Table of All CPE's in SRTool"""

    def __init__(self, *args, **kwargs):
        super(CpesTable, self).__init__(*args, **kwargs)
        self.default_orderby = "vulnerable"

    def get_context_data(self, **kwargs):
        context = super(CpesTable, self).get_context_data(**kwargs)
        return context

    def setup_filters(self, *args, **kwargs):
        # Is Vulnerable filter
        is_vulnerable = TableFilter(name="is_vulnerable",
                                       title="Filter CPE's by 'Vulnerable")

        exec_is_vulnerable = TableFilterActionToggle(
            "vulnerable",
            "Is Vulnerable",
            Q(vulnerable=True))

        exec_is_not_vulnerable = TableFilterActionToggle(
            "not_vulnerable",
            "Not Vulnerable",
            Q(vulnerable=False))

        is_vulnerable.add_action(exec_is_vulnerable)
        is_vulnerable.add_action(exec_is_not_vulnerable)
        self.add_filter(is_vulnerable)


    def setup_queryset(self, *args, **kwargs):
        self.queryset = \
                CpeTable.objects.all()

        self.queryset = self.queryset.order_by(self.default_orderby)

    def setup_columns(self, *args, **kwargs):

#    cpeMatchString = models.TextField(blank=True)
#    cpe23Uri = models.TextField(blank=True)

        self.add_column(title="Vulnerable",
                        field_name="vulnerable",
                        hideable=False,
                        orderable=True,
                        filter_name="is_vulnerable",
                        static_data_name="vulnerable",
                        static_data_template="{% if data.vulnerable %}Yes{% else %}No{% endif %}",
                        )

        self.add_column(title="CPE 2.3",
                        field_name="cpe23Uri",
                        hideable=False,
                        orderable=True,
                        )

        self.add_column(title="CPE 2.2",
                        field_name="cpeMatchString",
                        hideable=True,
                        hidden=True,
                        orderable=True,
                        )

        self.add_column(title="Version End",
                        field_name="versionEndIncluding",
                        hideable=True,
                        orderable=False,
                        help_text="Version End Including",
                        )

        cve_link_template = '''
            {% for pv in data.cpe2cve.all %}
              {% if not forloop.first %} {% endif %}<a href="{% url 'cve' pv.cve.name %}">{{pv.cve.name}} </a>
            {% endfor %}
        '''
        self.add_column(title="CVE",
                        hideable=False,
                        field_name="cveName",
                        static_data_name="cveName",
                        static_data_template=cve_link_template,
                        )


class CpesSrtoolTable(ToasterTable):
    """Table of All SRTool CPE's"""

    def __init__(self, *args, **kwargs):
        super(CpesSrtoolTable, self).__init__(*args, **kwargs)
        self.default_orderby = "name"

    def get_context_data(self, **kwargs):
        context = super(CpesSrtoolTable, self).get_context_data(**kwargs)
        return context

    def setup_queryset(self, *args, **kwargs):
        self.queryset = \
                Package.objects.filter(mode=Package.FOR)

        self.queryset = self.queryset.order_by(self.default_orderby)

    def setup_columns(self, *args, **kwargs):

        self.add_column(title="Name",
                        field_name="name",
                        hideable=False,
                        orderable=True,
                        static_data_template='''{% if 0 == data.cve_count %}{{data.name}}{% else %}<a href="{% url 'package-filter-detail'%}?package_name={{data.name}}">{{data.name}}</a>{% endif %}''',
                        )

        self.add_column(title="Cve Count",
                        field_name="cve_count",
                        hideable=False,
                        orderable=True,
                        static_data_name="cve_count",
                        static_data_template='''{% if 0 == data.cve_count %}0{% else %}<a href="{% url 'package-filter-detail'%}?package_name={{data.name}}">{{data.cve_count}}</a>{% endif %}''',
                        )

        self.add_column(title="Vulnerabilitie Count",
                        field_name="vulnerability_count",
                        hideable=False,
                        orderable=True,
                        static_data_name="vulnerability_count",
                        static_data_template='''{% if 0 == data.vulnerability_count %}0{% else %}<a href="{% url 'package-filter-detail'%}?package_name={{data.name}}">{{data.vulnerability_count}}</a>{% endif %}''',
                        )

        self.add_column(title="Investigation Count",
                        field_name="investigation_count",
                        hideable=False,
                        orderable=True,
                        static_data_name="investigation_count",
                        static_data_template='''{% if 0 == data.investigation_count %}0{% else %}<a href="{% url 'package-filter-detail'%}?package_name={{data.name}}">{{data.investigation_count}}</a>{% endif %}''',
                        )

        self.add_column(title="Defect Count",
                        field_name="defect_count",
                        hideable=False,
                        orderable=True,
                        static_data_name="defect_count",
                        static_data_template='''{% if 0 == data.defect_count %}0{% else %}<a href="{% url 'package-filter-detail'%}?package_name={{data.name}}">{{data.defect_count}}</a>{% endif %}''',
                        )


class ManageCpeTable(ToasterTable):
    """Table of Filter CPE's in SRTool"""

    def __init__(self, *args, **kwargs):
        super(ManageCpeTable, self).__init__(*args, **kwargs)
        self.default_orderby = "key_prime"

    def get_context_data(self, **kwargs):
        context = super(ManageCpeTable, self).get_context_data(**kwargs)
        return context

    def setup_filters(self, *args, **kwargs):
        # Is prime filter
        is_prime = TableFilter(name="is_prime",
                                       title="Filter CPE's by Company")
        exec_is_prime = TableFilterActionToggle(
            "company",
            "Company",
            Q(key_sub=''))
        is_prime.add_action(exec_is_prime)
        self.add_filter(is_prime)

        # Status filter
        is_status = TableFilter(name="is_status",
                                       title="Filter CPE's by Status")
        exec_is_undecided = TableFilterActionToggle(
            "undecided",
            "Undecided",
            Q(status=CpeFilter.UNDECIDED))
        is_status.add_action(exec_is_undecided)
        exec_is_include = TableFilterActionToggle(
            "include",
            "Include",
            Q(status=CpeFilter.INCLUDE))
        is_status.add_action(exec_is_include)
        exec_is_exclude = TableFilterActionToggle(
            "exclude",
            "Exclude",
            Q(status=CpeFilter.EXCLUDE))
        is_status.add_action(exec_is_exclude)
        exec_is_manual = TableFilterActionToggle(
            "manual",
            "Manual",
            Q(status=CpeFilter.MANUAL))
        is_status.add_action(exec_is_manual)
        self.add_filter(is_status)


    def setup_queryset(self, *args, **kwargs):
        self.queryset = \
                CpeFilter.objects.all()

        self.queryset = self.queryset.order_by(self.default_orderby)

    def setup_columns(self, *args, **kwargs):


        self.add_column(title="select",
                        field_name="Select",
                        hideable=False,
                        static_data_name="select",
                        static_data_template='<input type="checkbox" name="check_{{data.key_prime}}_{{data.key_sub}}" />',
                        )


        self.add_column(title="status",
                        field_name="status",
                        hideable=False,
                        orderable=True,
                        filter_name="is_status",
                        static_data_name="status",
                        static_data_template="{{data.get_status_text}}",
                        )

        self.add_column(title="Company",
                        field_name="key_prime",
                        filter_name="is_prime",
                        hideable=False,
                        orderable=True,
                        )

        self.add_column(title="Product",
                        field_name="key_sub",
                        hideable=False,
                        orderable=True,
                        static_data_name="key_sub",
                        static_data_template="{% if data.key_sub %}{{data.key_sub}}{% else %}(company){% endif %}",
                        )

        self.add_column(title="Automatic",
                        field_name="automatic",
                        hideable=False,
                        orderable=True,
                        static_data_name="automatic",
                        static_data_template="{% if data.automatic %}Yes{% else %}No{% endif %}",
                        )

        manage_link_template = '''
            <span class="glyphicon glyphicon-edit js-icon-pencil-config_var" id="affected_edit_'+{{data.id}}+'" x-data="'+{{data.id}}+'"></span>
            <span class="glyphicon glyphicon-trash js-icon-trash-config_var" id="comment_trash_'+{{data.id}}+'" x-data="'+{{data.id}}+'"></span>
        '''
        self.add_column(title="Manage",
                        field_name="manage",
                        hideable=False,
                        orderable=True,
                        static_data_name="manage",
                        static_data_template=manage_link_template,
                        )



class ProductsTable(ToasterTable):
    """Table of All Products in SRTool"""

    def __init__(self, *args, **kwargs):
        super(ProductsTable, self).__init__(*args, **kwargs)
        self.default_orderby = "order"

    def get_context_data(self, **kwargs):
        context = super(ProductsTable, self).get_context_data(**kwargs)
        return context


    def setup_queryset(self, *args, **kwargs):
        self.queryset = \
                Product.objects.all()

        self.queryset = self.queryset.order_by(self.default_orderby)

    def setup_filters(self, *args, **kwargs):
        # Execution outcome types filter
        investigations = TableFilter(name="investigations",
                                       title="Filter Investigations by status")

        exec_investigation_action_open = TableFilterActionToggle(
            "open",
            "Open",
            Q(status=1))

        exec_investigation_action_open_high = TableFilterActionToggle(
            "not_executed",
            "Open High Priority",
            Q(status=1))

        investigations.add_action(exec_investigation_action_open)
        investigations.add_action(exec_investigation_action_open_high)
        self.add_filter(investigations)

    def setup_columns(self, *args, **kwargs):

        self.add_column(title="Order",
                        field_name="order",
                        hideable=False,
                        orderable=True,
                        )

        key_link_template = '''
            <a href="{% url 'product' data.id %}" id="dataid_{{data.id}}">{{data.key}}<a>
        '''
        self.add_column(title="Key",
                        hideable=False,
                        orderable=True,
                        field_name="key",
                        static_data_name="key",
                        static_data_template=key_link_template,
                        )

        self.add_column(title="Name",
                        field_name="name",
                        hideable=False,
                        )

        self.add_column(title="Version",
                        field_name="version",
                        hideable=False,
                        )

        self.add_column(title="Extension",
                        field_name="profile",
                        hideable=False,
                        )

        self.add_column(title="CPE",
                        field_name="cpe",
                        hideable=True,
                        hidden=True,
                        )

        self.add_column(title="Defect Tags",
                        field_name="defect_tags",
                        hideable=False,
                        )

        self.add_column(title="Product Tags",
                        field_name="product_tags",
                        hideable=False,
                        )


        investigations_link_template = '''
            {% if data.product_investigation.all.count %}
                <a href="{% url 'investigations' %}?filter=is_product:{{data.key}}&">
                    {{data.product_investigation.all.count}}
                </a>
            {% else %}0{% endif %}
        '''
        self.add_column(title="Investigations",
                    field_name="investigations",
                    hidden=False,
                    static_data_name="investigations",
                    static_data_template=investigations_link_template,
                    )

        defects_link_template = '''
            {% if data.product_defect.all.count %}
                <a href="{% url 'defects' %}?filter=is_product:{{data.key}}&">
                    {{data.product_defect.all.count}}
                </a>
            {% else %}0{% endif %}
        '''
        self.add_column(title="Defects",
                    field_name="defects",
                    hidden=False,
                    static_data_name="defects",
                    static_data_template=defects_link_template,
                    )


class VulnerabilitiesTable(ToasterTable):
    """Table of All Vulnerabilities in SRTool"""

    def __init__(self, *args, **kwargs):
        super(VulnerabilitiesTable, self).__init__(*args, **kwargs)
        self.default_orderby = "name"

    def get_context_data(self, **kwargs):
        context = super(VulnerabilitiesTable, self).get_context_data(**kwargs)
        return context


    def setup_filters(self, *args, **kwargs):
        # Status filter
        is_status = TableFilter(name="is_status",
                                       title="Filter Vulnerabilities by 'Status'")
        exec_is_investigate = TableFilterActionToggle(
            "investigate",
            "Investigate",
            Q(status=Vulnerability.INVESTIGATE))
        exec_is_vulnerable = TableFilterActionToggle(
            "vulnerable",
            "Is Vulnerable",
            Q(status=Vulnerability.VULNERABLE))
        exec_is_not_vulnerable = TableFilterActionToggle(
            "not_vulnerable",
            "Not Vulnerable",
            Q(status=Vulnerability.NOT_VULNERABLE))
        is_status.add_action(exec_is_investigate)
        is_status.add_action(exec_is_vulnerable)
        is_status.add_action(exec_is_not_vulnerable)
        self.add_filter(is_status)

        # Outcome filter
        is_outcome = TableFilter(name="is_outcome",
                                       title="Filter Vulnerabilities by 'Outcome'")
        exec_is_open = TableFilterActionToggle(
            "open",
            "Open",
            Q(outcome=Vulnerability.OPEN))
        exec_is_closed = TableFilterActionToggle(
            "closed",
            "Closed",
            Q(outcome=Vulnerability.CLOSED))
        exec_is_fixed = TableFilterActionToggle(
            "fixed",
            "Fixed",
            Q(outcome=Vulnerability.FIXED))
        exec_is_not_fix = TableFilterActionToggle(
            "not_fix",
            "Won't Fix",
            Q(outcome=Vulnerability.NOT_FIX))
        is_outcome.add_action(exec_is_open)
        is_outcome.add_action(exec_is_closed)
        is_outcome.add_action(exec_is_fixed)
        is_outcome.add_action(exec_is_not_fix)
        self.add_filter(is_outcome)

        # Priority filter
        is_priority = TableFilter(name="is_priority",
                                       title="Filter Vulnerabilities by 'Priority'")
        exec_is_low = TableFilterActionToggle(
            "low",
            "Low",
            Q(priority=Vulnerability.LOW))
        exec_is_medium = TableFilterActionToggle(
            "medium",
            "Medium",
            Q(priority=Vulnerability.MEDIUM))
        exec_is_high = TableFilterActionToggle(
            "high",
            "High",
            Q(priority=Vulnerability.HIGH))
        is_priority.add_action(exec_is_low)
        is_priority.add_action(exec_is_medium)
        is_priority.add_action(exec_is_high)
        self.add_filter(is_priority)

    def setup_queryset(self, *args, **kwargs):
        self.queryset = \
                Vulnerability.objects.all()

        # filter out hidden records
        if not UserSafe.is_admin(self.request.user):
            self.queryset = self.queryset.exclude(public = False)

        self.queryset = self.queryset.order_by(self.default_orderby)

    def setup_columns(self, *args, **kwargs):

        id_link_template = '''
            <a href="{% url 'vulnerability' data.name %}" id="dataid_{{data.id}}">{{data.name}}</a>
        '''
        self.add_column(title="ID",
                        hideable=False,
                        orderable=True,
                        field_name="name",
                        static_data_name="name",
                        static_data_template=id_link_template,
                        )

        # !!! HACK: 'vc.cve.name' is returning '%s' when it is supposed to be null !!!
        cve_link_template = '''
            {% for vc in data.vulnerability_to_cve.all %}
              {% if vc.cve.name != '%s' %}
                {% if not forloop.first %} {% endif %} <a href="{% url 'cve' vc.cve.name %}">{{vc.cve.name}}</a>
              {% endif %}
            {% endfor %}
        '''
        self.add_column(title="CVE",
                        hideable=False,
                        orderable=False,
                        field_name="cve__name",
                        static_data_name="cve__name",
                        static_data_template=cve_link_template,
                        )

        self.add_column(title="Status",
                        field_name="status",
                        hideable=False,
                        orderable=True,
                        filter_name="is_status",
                        static_data_name="status",
                        static_data_template="{{data.get_status_text}}",
                        )

        self.add_column(title="Outcome",
                        field_name="outcome",
                        hideable=False,
                        orderable=True,
                        filter_name="is_outcome",
                        static_data_name="outcome",
                        static_data_template="{{data.get_outcome_text}}",
                        )

        self.add_column(title="Priority",
                        field_name="priority",
                        hideable=False,
                        orderable=True,
                        filter_name="is_priority",
                        static_data_name="priority",
                        static_data_template="{{data.get_priority_text}}",
                        )

        self.add_column(title="Comments",
                        field_name="comments",
                        hideable=True,
                        )

        if UserSafe.is_admin(self.request.user):
            self.add_column(title="Comments Private",
                            field_name="comments_private",
                            hideable=True,
                            )

        investigate_link_template = '''
            {% for investigation in data.vulnerability_investigation.all %}
              {% if not forloop.first %} {% endif %}<a href="{% url 'investigation' investigation.name %}" target="_blank">{{investigation.name}}</a>
            {% endfor %}
        '''
        self.add_column(title="Investigations",
                        orderable=False,
                        static_data_name="vulnerability_investigation",
                        static_data_template=investigate_link_template,
                        hidden=False,
                        )

        defect_link_template = '''
            {% for investigation in data.vulnerability_investigation.all %}
              {% for id in investigation.investigation_to_defect.all %}
                {% if forloop.counter == 1 %}| {% endif %}<a href="{% url 'defect_name' id.defect.name %}" target="_blank">{{id.defect.name}}</a>
              {% endfor %}
            {% endfor %}
        '''
        self.add_column(title="Defects",
                        orderable=False,
                        static_data_name="investigation_to_defect",
                        static_data_template=defect_link_template,
                        hidden=False,
                        )

       # VulnerabilityProduct.AFFECTED = 0
        product_link_template = '''
            {% for vp in data.affected_products.all %}
              {% if vp.product != '%s' %}
                {% if vp.relation == 0 %}{% if not forloop.first %} {% endif %}<a href="{% url 'product' vp.product.id %}" target="_blank">{{vp.product.key}}</a>{% endif %}
              {% endif %}
            {% endfor %}
        '''
        self.add_column(title="Products",
                        orderable=False,
                        static_data_name="investigation_products",
                        static_data_template=product_link_template,
                        hidden=False,
                        )


class InvestigationsTable(ToasterTable):
    """Table of All Investigations in SRTool"""

    def __init__(self, *args, **kwargs):
        super(InvestigationsTable, self).__init__(*args, **kwargs)
        self.default_orderby = "name"

    def get_context_data(self, **kwargs):
        context = super(InvestigationsTable, self).get_context_data(**kwargs)
        return context

    def setup_filters(self, *args, **kwargs):
        # Status filter
        is_status = TableFilter(name="is_status",
                                       title="Filter Investigations by 'Status'")
        is_status.add_action(TableFilterActionToggle(
            "investigate",
            "Investigate",
            Q(status=Investigation.INVESTIGATE))
            )
        is_status.add_action(TableFilterActionToggle(
            "vulnerable",
            "Is Vulnerable",
            Q(status=Investigation.VULNERABLE))
            )
        is_status.add_action(TableFilterActionToggle(
            "not_vulnerable",
            "Not Vulnerable",
            Q(status=Investigation.NOT_VULNERABLE))
            )
        self.add_filter(is_status)

        # Outcome filter
        is_outcome = TableFilter(name="is_outcome",
                                       title="Filter Investigations by 'Outcome'")
        exec_is_open = TableFilterActionToggle(
            "open",
            "Open",
            Q(outcome=Investigation.OPEN))
        exec_is_closed = TableFilterActionToggle(
            "closed",
            "Closed",
            Q(outcome=Investigation.CLOSED))
        exec_is_fixed = TableFilterActionToggle(
            "fixed",
            "Fixed",
            Q(outcome=Investigation.FIXED))
        exec_is_not_fix = TableFilterActionToggle(
            "not_fix",
            "Won't Fix",
            Q(outcome=Investigation.NOT_FIX))
        is_outcome.add_action(exec_is_open)
        is_outcome.add_action(exec_is_closed)
        is_outcome.add_action(exec_is_fixed)
        is_outcome.add_action(exec_is_not_fix)
        self.add_filter(is_outcome)

        # Priority filter
        is_priority = TableFilter(name="is_priority",
                                       title="Filter Investigations by 'Priority'")
        exec_is_low = TableFilterActionToggle(
            "low",
            "Low",
            Q(priority=Investigation.LOW))
        exec_is_medium = TableFilterActionToggle(
            "medium",
            "Medium",
            Q(priority=Investigation.MEDIUM))
        exec_is_high = TableFilterActionToggle(
            "high",
            "High",
            Q(priority=Investigation.HIGH))
        is_priority.add_action(exec_is_low)
        is_priority.add_action(exec_is_medium)
        is_priority.add_action(exec_is_high)
        self.add_filter(is_priority)

        # Product filter
        is_product = TableFilter(name="is_product",
                                       title="Filter Investigations by 'Product'")
        for p in Product.objects.all():
            is_product.add_action( TableFilterActionToggle(
                p.key,
                p.long_name,
                Q(product=p)) )
        self.add_filter(is_product)


    def setup_queryset(self, *args, **kwargs):
        self.queryset = \
                Investigation.objects.all()

        # filter out hidden records
        if not UserSafe.is_admin(self.request.user):
            self.queryset = self.queryset.exclude(public = False)

        self.queryset = self.queryset.order_by(self.default_orderby)

    def setup_columns(self, *args, **kwargs):

        id_link_template = '''
            <a href="{% url 'investigation' data.name %}" id="dataid_{{data.id}}">{{data.name}}</a>
        '''
        self.add_column(title="ID",
                        hideable=False,
                        orderable=True,
                        field_name="name",
                        static_data_name="name",
                        static_data_template=id_link_template,
                        )

        defect_link_template = '''
            {% for ij in data.investigation_to_defect.all %}
              {% if not forloop.first %} {% endif %}<a href="{% url 'defect_name' ij.defect.name %}">{{ij.defect.name}} </a>
            {% endfor %}
        '''
        self.add_column(title="Defects",
                        field_name="defect",
                        hideable=False,
                        orderable=False,
                        static_data_name="defect",
                        static_data_template=defect_link_template,
                        )

        self.add_column(title="Status",
                        field_name="status",
                        hideable=True,
                        orderable=True,
                        filter_name="is_status",
                        static_data_name="status",
                        static_data_template="{{data.get_status_text}}",
                        )

        self.add_column(title="Outcome",
                        field_name="outcome",
                        hideable=False,
                        orderable=True,
                        filter_name="is_outcome",
                        static_data_name="outcome",
                        static_data_template="{{data.get_outcome_text}}",
                        )

        release_version_template = '''
            {% for ij in data.investigation_to_defect.all %}
              {% if not forloop.first %} {% endif %}<a href="{{ij.defect.url}}" target="_blank">{{ij.defect.release_version}} </a>
            {% endfor %}
        '''
        self.add_column(title="Release Version",
                        hideable=False,
                        orderable=False,
                        static_data_name="release_version",
                        static_data_template=release_version_template,
                        )

        self.add_column(title="Priority",
                        field_name="priority",
                        filter_name="is_priority",
                        hideable=False,
                        orderable=True,
                        static_data_name="priority",
                        static_data_template="{{data.get_priority_text}}",
                        )

        self.add_column(title="Comments",
                        field_name="comments",
                        hideable=True,
                        )

        if UserSafe.is_admin(self.request.user):
            self.add_column(title="Comments Private",
                            field_name="comments_private",
                            hideable=True,
                            )

        self.add_column(title="Vulnerability",
                        hidden=False,
                        orderable=False,
                        static_data_name="vulnerability",
                        static_data_template='''<a href="{% url 'vulnerability' data.vulnerability.name %}">{{data.vulnerability.get_long_name}}</a>''',
                        )

        self.add_column(title="Product",
                        hidden=False,
#                        orderable=True,
                        filter_name="is_product",
                        static_data_name="investigation_products",
                        static_data_template="<a href=\"{% url 'product' data.product.id %}\">{{data.product.long_name}}</a>",
                        )


class SourcesTable(ToasterTable):
    """Table of All Data Sources in SRTool"""

    def __init__(self, *args, **kwargs):
        super(SourcesTable, self).__init__(*args, **kwargs)
        self.default_orderby = "key"

    def get_context_data(self, **kwargs):
        context = super(SourcesTable, self).get_context_data(**kwargs)
        return context

    def setup_queryset(self, *args, **kwargs):
        self.queryset = \
                DataSource.objects.all()

        self.queryset = self.queryset.order_by(self.default_orderby)

    def setup_columns(self, *args, **kwargs):

        self.add_column(title="Key",
                        hideable=False,
                        orderable=True,
                        field_name="key",
                        )

        self.add_column(title="Data",
                        hideable=False,
                        orderable=True,
                        field_name="data",
                        static_data_name="data",
                        static_data_template='<span id="dataid_{{data.id}}">{{data.data}}</span>',
                        )

        self.add_column(title="Source",
                        hideable=False,
                        orderable=True,
                        field_name="source",
                        )

        self.add_column(title="Name",
                        hideable=False,
                        orderable=False,
                        field_name="name",
                        )

        self.add_column(title="Attributes",
                        hideable=True,
                        hidden=True,
                        orderable=False,
                        field_name="attributes",
                        )

        self.add_column(title="Description",
                        hideable=False,
                        orderable=False,
                        field_name="description",
                        )

        self.add_column(title="Init Action",
                        hideable=True,
                        hidden=True,
                        orderable=False,
                        field_name="init",
                        )

        self.add_column(title="Update Action",
                        hideable=True,
                        hidden=True,
                        orderable=False,
                        field_name="update",
                        )

        self.add_column(title="Lookup Action",
                        hideable=True,
                        hidden=True,
                        orderable=False,
                        field_name="lookup",
                        )

        self.add_column(title="Data Modified",
                        help_text="Last upstream date",
                        hideable=False,
                        orderable=True,
                        field_name="lastModifiedDate",
                        )

        updated_template = '''
          {% if data.update %}{{data.lastUpdatedDate}}{% else %}---{% endif %}
        '''
        self.add_column(title="Updated",
                        help_text="Last update date",
                        hideable=True,
                        hidden=True,
                        #orderable=True,
                        static_data_name="updated",
                        static_data_template=updated_template,
                        )

        frequency_template = '''
          {% if data.update %}{{data.get_frequency_text}}{% else %}({{data.get_frequency_text}}){% endif %}
        '''
        self.add_column(title="Update Freq.",
                        hideable=False,
                        orderable=True,
                        field_name="update_frequency",
                        static_data_name="update_frequency",
                        static_data_template=frequency_template,
                        )

        self.add_column(title="Update Detail",
                        hideable=True,
                        hidden=True,
                        field_name="update_time",
                        )

        self.add_column(title="Loaded",
                        hideable=True,
                        hidden=True,
                        field_name="loaded",
                        )
        self.add_column(title="CVE Filter",
                        hideable=True,
                        hidden=True,
                        field_name="cve_filter",
                        )


class SelectPublishTable(ToasterTable):
    """Table of Publishable CVE's in SRTool"""

    def __init__(self, *args, **kwargs):
        super(SelectPublishTable, self).__init__(*args, **kwargs)
        self.default_orderby = "name"
        _log("SelectPublishTable:__init")

    def get_context_data(self,**kwargs):
        _log("SelectPublishTable:get_context_data")
        context = super(SelectPublishTable, self).get_context_data(**kwargs)
        return context

    def setup_filters(self, *args, **kwargs):
        _log("SelectPublishTable:setup_filters")
        # Status filter
        is_status = TableFilter(name="is_status",
                                       title="Filter CVE's by 'Status")
        is_status.add_action(TableFilterActionToggle(
            "new",
            "New",
            Q(status=Cve.NEW))
            )
        is_status.add_action(TableFilterActionToggle(
            "investigate",
            "Investigate",
            Q(status=Cve.INVESTIGATE))
            )
        is_status.add_action(TableFilterActionToggle(
            "vulnerable",
            "Is Vulnerable",
            Q(status=Cve.VULNERABLE))
            )
        is_status.add_action(TableFilterActionToggle(
            "not_vulnerable",
            "Not Vulnerable",
            Q(status=Cve.NOT_VULNERABLE))
            )
        self.add_filter(is_status)

    def setup_queryset(self, *args, **kwargs):
        _log("SelectPublishTable:setup_queryset")
        self.queryset = \
                Cve.objects.filter(publish_state = Cve.PUBLISH_REQUEST) | \
                Cve.objects.filter(publish_state = Cve.PUBLISH_UPDATE)

        _log("SelectPublishTable1:%s" % len(self.queryset))

        # filter out hidden records !!! ALL NEW ONES SHOULD BE PUBLIC
#        userAccess = Access(self.request.session.get('srt_user_id', '0'))
#        if not userAccess.is_admin():
#            self.queryset = self.queryset.exclude(public = False)

        self.queryset = self.queryset.order_by(self.default_orderby)

    def setup_columns(self, *args, **kwargs):

        _log("SelectPublishTable:setup_columns")

        self.add_column(title="Select",
                        field_name="Select",
                        hideable=False,
                        static_data_name="select",
                        static_data_template='<input type="checkbox" name="{{data.name}}" />',
                        )

        self.add_column(title="Status",
                        field_name="status",
                        hideable=False,
                        orderable=True,
                        filter_name="is_status",
                        static_data_name="status",
                        static_data_template="{{data.get_status_text}}",
                        )

        id_link_template = '''
            <a href="{% url 'cve' data.name %}" id="dataid_{{data.id}}" target="_blank">{{data.name}}</a>
        '''
        self.add_column(title="Name",
                        hideable=False,
                        orderable=True,
                        field_name="name",
                        static_data_name="name",
                        static_data_template=id_link_template,
                        )

        self.add_column(title="Publish Request",
                        field_name="publish_state",
                        hideable=False,
                        static_data_name="publish_state",
                        static_data_template="{{data.get_publish_text}}",
                        )

        self.add_column(title="Request Date",
                        field_name="publish_date",
                        hideable=False,
                        static_data_name="publish_date",
                        static_data_template="{% if data.publish_date %}{{data.publish_date}}{% else %}ASAP{% endif %}",
                        )

        priority_v3_template = '''
          {{data.cvssV3_baseScore}} {{data.cvssV3_basePriority}}
        '''
        self.add_column(title="Priority (V3)",
                        help_text="Priority of the CVE (v3)",
                        hideable=False,
                        orderable=False,
                        static_data_name='priority_v3',
                        static_data_template=priority_v3_template,
                        )

        self.add_column(title="Description",
                        field_name="description",
                        hideable=False,
                        )

class UpdatePublishedTable(ToasterTable):
    """Table of Publish requested CVE's in SRTool"""

    def __init__(self, *args, **kwargs):
        super(UpdatePublishedTable, self).__init__(*args, **kwargs)
        self.default_orderby = "date"

    def get_context_data(self,**kwargs):
        context = super(UpdatePublishedTable, self).get_context_data(**kwargs)
        return context

    def setup_queryset(self, *args, **kwargs):
        self.queryset = PublishPending.objects.all()
        self.queryset = self.queryset.order_by(self.default_orderby)

    def setup_columns(self, *args, **kwargs):

        self.add_column(title="Select",
                        field_name="Select",
                        hideable=False,
                        static_data_name="select",
                        static_data_template='<input type="checkbox" name="{{data.cve.name}}" />',
                        )

        self.add_column(title="Status",
                        field_name="status",
                        hideable=False,
                        orderable=True,
                        static_data_name="status",
                        static_data_template="{{data.cve.get_status_text}}",
                        )

        id_link_template = '''
            <a href="{% url 'cve' data.cve.name %}" id="dataid_{{data.cve.id}}" target="_blank">{{data.cve.name}}</a>
        '''
        self.add_column(title="Name",
                        hideable=False,
                        orderable=True,
                        field_name="name",
                        static_data_name="name",
                        static_data_template=id_link_template,
                        )

        self.add_column(title="Publish Request",
                        field_name="publish_state",
                        hideable=False,
                        static_data_name="publish_state",
                        static_data_template="{{data.cve.get_publish_text}}",
                        )

        self.add_column(title="Publish Request Date",
                        field_name="date",
                        hideable=False,
                        orderable=True,
                        )

class NotificationsTable(ToasterTable):
    """Table of Notifications in SRTool"""

    def __init__(self, *args, **kwargs):
        super(NotificationsTable, self).__init__(*args, **kwargs)
        self.default_orderby = "-srt_created"

    def get_context_data(self,**kwargs):
        context = super(NotificationsTable, self).get_context_data(**kwargs)
        context['notify_categories'] = NotifyCategories.objects.all()
        context['users'] = UserSafe.get_safe_userlist(True)
        return context

    def setup_queryset(self, *args, **kwargs):
        self.queryset = Notify.objects.all()
        self.queryset = self.queryset.order_by(self.default_orderby)

    def setup_columns(self, *args, **kwargs):

        self.add_column(title="Select",
                        field_name="Select",
                        hideable=False,
                        static_data_name="select",
                        static_data_template='<input type="checkbox" value="{{data.pk}}" name="select-notify" />',
                        )

        self.add_column(title="SRT Created",
                        hideable=False,
                        orderable=True,
                        field_name="srt_created",
                        static_data_name="srt_created",
                        static_data_template='{{data.srt_updated | date:"m/d/y H:i"}}'
                        )

        self.add_column(title="Category",
                        field_name="category",
                        hideable=False,
                        orderable=True,
                        )

        self.add_column(title="Description",
                        field_name="description",
                        hideable=False,
                        orderable=True,
                        )

        self.add_column(title="Priority",
                        field_name="priority",
                        orderable=True,
                        static_data_name="priority",
                        static_data_template='''{{ data.get_priority_text }}''',
                        )

        self.add_column(title="URL",
                        field_name="url",
                        hideable=True,
                        orderable=True,
                        static_data_name="url",
                        static_data_template='''<a href="{{data.url}}" target="_blank">{{ data.url }}</a>''',
                        )

        self.add_column(title="Author",
                        field_name="author",
                        hideable=True,
                        hidden=True,
                        orderable=True,
                        )

        self.add_column(title="SRT Update",
                        hideable=True,
                        hidden=True,
                        orderable=True,
                        field_name="srt_updated",
                        static_data_name="srt_updated",
                        static_data_template='{{data.srt_updated | date:"m/d/y H:i"}}'
                        )

        users_link_template = '''
            {% for tu in data.todo2user.all %}
              {% if not forloop.first %}, {% endif %}{{tu.user.username}}</a>
            {% endfor %}
        '''
        self.add_column(title="Users",
                        hideable=True,
                        static_data_name="users",
                        static_data_template=users_link_template,
                        )

#        self.add_column(title="Author",
#                        hideable=True,
#                        static_data_name="author",
#                        static_data_template='''{{data.author.name}}''',
#                        )

        manage_link_template = '''
            <span class="glyphicon glyphicon-edit edit-notify" id="notify_edit_'+{{data.id}}+'" x-data="{{data.id}}"></span>
        '''
#            <span class="glyphicon glyphicon-trash trash-notify" id="notify_trash_'+{{data.id}}+'" x-data="{{data.id}}"></span>
        self.add_column(title="Manage",
            static_data_name="manage",
            static_data_template=manage_link_template,
            )


class PackageFilterTable(ToasterTable):
    """Table of Package Filters in SRTool"""

    def __init__(self, *args, **kwargs):
        super(PackageFilterTable, self).__init__(*args, **kwargs)
        # ?limit=25&page=1&orderby=name&filter=is_mode:for&default_orderby=mode&filter_value=on&
        self.default_orderby = "mode"
#        self.default_orderby = "name"
#        self.default_filter = "is_mode:for"

    def get_context_data(self,**kwargs):
        context = super(PackageFilterTable, self).get_context_data(**kwargs)
        return context

    def setup_filters(self, *args, **kwargs):
        # Mode filter
        is_mode = TableFilter(name="is_mode",
                                       title="mode")
        exec_for = TableFilterActionToggle(
            "for",
            "For",
            Q(mode=Package.FOR))
        exec_against = TableFilterActionToggle(
            "against",
            "Against",
            Q(mode=Package.AGAINST))
        is_mode.add_action(exec_for)
        is_mode.add_action(exec_against)
        self.add_filter(is_mode)

    def setup_queryset(self, *args, **kwargs):
        self.queryset = Package.objects.all()
        self.queryset = self.queryset.order_by(self.default_orderby)

    def setup_columns(self, *args, **kwargs):

        self.add_column(title="Select",
                        field_name="Select",
                        hideable=False,
                        static_data_name="select",
                        static_data_template='<input type="checkbox" value="{{data.pk}}" name="select-notify" />',
                        )

        self.add_column(title="Mode",
                        field_name="mode",
                        hideable=False,
                        filter_name="is_mode",
                        static_data_name="mode",
                        static_data_template='{{data.get_mode_text}}',
                        )

        self.add_column(title="Weight",
                        field_name="weight",
                        hideable=True,
                        hidden=True,
                        orderable=True,
                        )

        self.add_column(title="Name",
                        hideable=False,
                        orderable=True,
                        field_name="name",
                        )

        self.add_column(title="Real Name",
                        hideable=False,
                        orderable=True,
                        field_name="realname",
                        )

        self.add_column(title="Invalid Name",
                        hideable=False,
                        orderable=True,
                        field_name="invalidname",
                        )

        self.add_column(title="Cve Count",
                        field_name="cve_count",
                        hideable=False,
                        orderable=True,
                        static_data_name="cve_count",
                        static_data_template='''{% if 0 == data.cve_count %}0{% else %}<a href="{% url 'package-filter-detail'%}?package_name={{data.name}}">{{data.cve_count}}</a>{% endif %}''',
                        )

        self.add_column(title="Vulnerability Count",
                        field_name="vulnerability_count",
                        hideable=False,
                        orderable=True,
                        static_data_name="vulnerability_count",
                        static_data_template='''{% if 0 == data.vulnerability_count %}0{% else %}<a href="{% url 'package-filter-detail'%}?package_name={{data.name}}">{{data.vulnerability_count}}</a>{% endif %}''',
                        )

        self.add_column(title="Investigation Count",
                        field_name="investigation_count",
                        hideable=False,
                        orderable=True,
                        static_data_name="investigation_count",
                        static_data_template='''{% if 0 == data.investigation_count %}0{% else %}<a href="{% url 'package-filter-detail'%}?package_name={{data.name}}">{{data.investigation_count}}</a>{% endif %}''',
                        )

        self.add_column(title="Defect Count",
                        field_name="defect_count",
                        hideable=False,
                        orderable=True,
                        static_data_name="defect_count",
                        static_data_template='''{% if 0 == data.defect_count %}0{% else %}<a href="{% url 'package-filter-detail'%}?package_name={{data.name}}">{{data.defect_count}}</a>{% endif %}''',
                        )

        manage_link_template = '''
            <span class="glyphicon glyphicon-edit js-icon-pencil-config_var" id="affected_edit_'+{{data.id}}+'" x-data="'+{{data.id}}+'"></span>
        '''
        self.add_column(title="Manage",
                        hideable=False,
                        orderable=False,
                        static_data_name="manage",
                        static_data_template=manage_link_template,
                        )

class PackageFilterDetailTable(ToasterTable):
    """Table of Package Filter Details in SRTool"""

    def __init__(self, *args, **kwargs):
        super(PackageFilterDetailTable, self).__init__(*args, **kwargs)
        self.default_orderby = "name"

    def get_context_data(self,**kwargs):
        context = super(PackageFilterDetailTable, self).get_context_data(**kwargs)
        package_name = self.request.GET.get('package_name','')
        context['package_name'] = package_name
        return context

    def setup_queryset(self, *args, **kwargs):
        package_name = self.request.GET.get('package_name','')
        package = Package.objects.get(name=package_name)
        queryset = package.package2cve.all()
        custom_list = [rec.cve.id for rec in queryset]
        self.queryset = Cve.objects.filter(id__in=custom_list)
        self.queryset = self.queryset.order_by(self.default_orderby)

    def setup_columns(self, *args, **kwargs):

        self.add_column(title="Cve",
                        field_name="name",
                        hideable=False,
                        orderable=True,
                        static_data_name="name",
                        static_data_template='''<a href="{% url 'cve' data.name %}">{{data.name}}</a>''',
                        )

        self.add_column(title="Status",
                        field_name="status",
                        hideable=False,
                        orderable=True,
                        static_data_name="status",
                        static_data_template='''{{ data.get_status_text }}''',
                        )

        self.add_column(title="Description",
                        field_name="description",
                        hideable=False,
                        static_data_name="description",
                        static_data_template='''{{ data.description|truncatechars:100 }}''',
                        )

        vulnerability_link_template = '''
            {% for cv in data.cve_to_vulnerability.all %}
              {% if not forloop.first %} {% endif %}<a href="{% url 'vulnerability' cv.vulnerability.name %}">{{cv.vulnerability.name}}</a>
            {% endfor %}
        '''
        self.add_column(title="Vulnerability",
                        hideable=False,
                        static_data_name="vulnerability",
                        static_data_template=vulnerability_link_template,
                        )

        investigation_link_template = '''
            {% for cv in data.cve_to_vulnerability.all %}
              {% for investigation in cv.vulnerability.vulnerability_investigation.all %}
                {% if not forloop.first %} {% endif %}<a href="{% url 'investigation' investigation.name %}">{{investigation.name}}</a>
              {% endfor %}
            {% endfor %}
        '''
        self.add_column(title="Investigations",
                        hideable=False,
                        static_data_name="investigations",
                        static_data_template=investigation_link_template,
                        )

        defect_link_template = '''
            {% for cv in data.cve_to_vulnerability.all %}
              {% for investigation in cv.vulnerability.vulnerability_investigation.all %}
                {% for id in investigation.investigation_to_defect.all %}
                  {% if not forloop.first %} {% endif %}<a href="{% url 'defect_name' id.defect.name %}">{{id.defect.name}}</a>
                {% endfor %}
              {% endfor %}
            {% endfor %}
        '''
        self.add_column(title="Defects",
                        hideable=False,
                        static_data_name="defects",
                        static_data_template=defect_link_template,
                        )
