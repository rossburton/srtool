#
# Security Response Tool Implementation
#
# Copyright (C) 2017-2018 Wind River Systems
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from django.conf.urls import url
from django.views.generic import RedirectView

from srtgui import tables
from srtgui import views

urlpatterns = [
        # landing page
        url(r'^landing/$', views.landing, name='landing'),

        url(r'^cve/(?P<cve_pk>\d+)$', views.cve, name="cve"),
        url(r'^cve/(?P<cve_pk>[^\d].+)$', views.cve, name="cve"),
        url(r'^cve-edit/(?P<cve_pk>[^\d].+)$', views.cve_edit, name="cve_edit"),
        url(r'^cve/(?P<cve_pk>\d+)/active_tab/(?P<active_tab>\d{1})$', views.cve, name="cve"),
        url(r'^cve/(?P<cve_pk>\d+)/active_tab/(?P<active_tab>[^\d].+)$', views.cve, name="cve"),
        url(r'^cves/$',
            tables.CvesTable.as_view(template_name="cves-toastertable.html"),
            name='cves'),
        url(r'^select-cves/$',
            tables.SelectCveTable.as_view(template_name="cves-select-toastertable.html"),
            name='select-cves'),
        url(r'^select-cves/(?P<cve_status>\d+)$',
            tables.SelectCveTable.as_view(template_name="cves-select-toastertable.html"),
            name='select-cves'),
        url(r'^cve-create/$', views.cve_create, name="cve_create"),
        url(r'^cve-alternates/(?P<cve_pk>\d+)$', views.cve_alternates, name="cve_alternates"),

        url(r'^defect/(?P<defect_pk>\d+)$', views.defect, name="defect"),
        url(r'^defect/(?P<defect_pk>[^\d].+)$', views.defect, name="defect_name"),
        url(r'^defects/$',
            tables.DefectsTable.as_view(template_name="defects-toastertable.html"),
            name='defects'),

        url(r'^cpes/$',
            tables.CpesTable.as_view(template_name="cpes-toastertable.html"),
            name='cpes'),
        url(r'^cpes-srtool/$',
            tables.CpesSrtoolTable.as_view(template_name="cpes-srtool-toastertable.html"),
            name='cpes_srtool'),

        url(r'^cwes/$',
            tables.CwesTable.as_view(template_name="cwes-toastertable.html"),
            name='cwes'),
        url(r'^cwe-cves/$',
            tables.CweCvesTable.as_view(template_name="cwecves-toastertable.html"),
            name='cwe_cves'),

        url(r'^product/(?P<product_pk>\d+)$', views.product, name="product"),
        url(r'^products/$',
            tables.ProductsTable.as_view(template_name="products-toastertable.html"),
            name='products'),

        url(r'^vulnerability/(?P<vulnerability_pk>\d+)$', views.vulnerability, name="vulnerability"),
        url(r'^vulnerability/(?P<vulnerability_pk>[^\d].+)$', views.vulnerability, name="vulnerability"),
        url(r'^vulnerabilities/$',
            tables.VulnerabilitiesTable.as_view(template_name="vulnerabilities-toastertable.html"),
            name='vulnerabilities'),
        url(r'^vulnerability-create/$', views.vulnerability_create, name="vulnerability_create"),

        url(r'^investigation/(?P<investigation_pk>\d+)$', views.investigation, name="investigation"),
        url(r'^investigation/(?P<investigation_pk>[^\d].+)$', views.investigation, name="investigation"),
        url(r'^investigations/$',
            tables.InvestigationsTable.as_view(template_name="investigations-toastertable.html"),
            name='investigations'),

        url(r'^package-filters/$',
            tables.PackageFilterTable.as_view(template_name="package-filters-toastertable.html"),
            name='package-filters'),
        url(r'^package-filter-detail/$',
            tables.PackageFilterDetailTable.as_view(template_name="package-filter-detail-toastertable.html"),
            name='package-filter-detail'),

        url(r'^select-publish/$',
            tables.SelectPublishTable.as_view(template_name="publish-select-toastertable.html"),
            name='select-publish'),

        url(r'^update-published/$',
            tables.UpdatePublishedTable.as_view(template_name="published-select-toastertable.html"),
            name='update-published'),

        url(r'^report/(?P<page_name>\D+)$', views.report, name='report'),


        url(r'^xhr_triage_commit/$', views.xhr_triage_commit,
            name='xhr_triage_commit'),

        url(r'^xhr_cve_commit/$', views.xhr_cve_commit,
            name='xhr_cve_commit'),

        url(r'^xhr_vulnerability_commit/$', views.xhr_vulnerability_commit,
            name='xhr_vulnerability_commit'),

        url(r'^xhr_investigation_commit/$', views.xhr_investigation_commit,
            name='xhr_investigation_commit'),

        url(r'^xhr_cve_publish_commit/$', views.xhr_cve_publish_commit,
            name='xhr_cve_publish_commit'),

        url(r'^xhr_notifications/$', views.xhr_notifications,
            name='xhr_notifications'),

        url(r'^xhr_packages/$', views.xhr_packages,
            name='xhr_packages'),


        url(r'^manage/$', views.management, name='manage'),
        url(r'^manage_cpes/$',
            tables.ManageCpeTable.as_view(template_name="manage-cpes-toastertable.html"),
            name='manage_cpes'),
        url(r'^notifications/$',
            tables.NotificationsTable.as_view(template_name="notifications-toastertable.html"),
            name='manage_notifications'),
        url(r'^triage_cves/$', views.triage_cves, name='triage_cves'),
        url(r'^create_vulnerability/$', views.create_vulnerability, name='create_vulnerability'),
        url(r'^publish/$', views.publish, name='publish'),
        url(r'^manage_report/$', views.manage_report, name='manage_report'),
        url(r'^sources/$',
            tables.SourcesTable.as_view(template_name="sources-toastertable.html"),
            name='sources'),
        url(r'^users/$', views.users, name='users'),


        url(r'^guided_tour/$', views.guided_tour, name='guided_tour'),

        url(r'^quicklink/$', views.quicklink, name='quicklink'),

        url(r'^tbd/$', views.tbd, name='tbd'),

        # default redirection
        url(r'^$', RedirectView.as_view(url='landing', permanent=True)),
]
