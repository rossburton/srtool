from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, PasswordChangeForm
from .models import SrtUser

class SrtUserCreationForm(UserCreationForm):

    class Meta(UserCreationForm):
        model = SrtUser
        fields = ('username', 'first_name', 'last_name', 'email', 'role' )

class SrtUserChangeForm(UserChangeForm):

    class Meta:
        model = SrtUser
        fields = ('username', 'email')

class SrtPasswordChangeForm(PasswordChangeForm):

    def __init__(self, *args, **kwargs):
        request = kwargs.pop("request") # it's best you pop request, so that you don't get any complains for a parent that checks what kwargs it gets
        super(SrtPasswordChangeForm, self).__init__(request.user, *args, **kwargs)

    class Meta:
        model = SrtUser
        fields = ('username')

