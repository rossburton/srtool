#
# ex:ts=4:sw=4:sts=4:et
# -*- tab-width: 4; c-basic-offset: 4; indent-tabs-mode: nil -*-
#
# Security Response Tool Implementation
#
# Copyright (C) 2017-2018 Wind River Systems
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import json
import traceback

from django.urls import reverse_lazy
from django.views import generic
from django.http import HttpResponse, HttpResponseNotFound, JsonResponse, HttpResponseRedirect

from django.contrib.auth.forms import UserCreationForm, UserChangeForm, PasswordChangeForm
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.models import Group
from django.shortcuts import render, redirect

from .models import SrtUser, UserSafe

from .forms import SrtUserCreationForm

# quick development/debugging support
from srtgui.api import _log

class SignUp(generic.CreateView):
    form_class = SrtUserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'

    # This method is called when valid form data has been POSTed.
    # It should return an HttpResponse.
    def form_valid(self, form):
        # Save the new category to the database.
        form.save(commit=True)
        # Preset new user to the "Reader" group
        new_user = SrtUser.objects.get(username=form.cleaned_data['username'])
        group = Group.objects.get(name = "Reader")
        group.user_set.add(new_user)
        return redirect(self.success_url)

#class PasswordChange(generic.CreateView):
#    form_class = SrtPasswordChangeForm
#    success_url = reverse_lazy('password_reset')
#    template_name = 'password_change_form.html'
#
#    def get_form_kwargs(self, **kwargs):
#        data = super(PasswordChange, self).get_form_kwargs(**kwargs)
#        data['request'] = self.request
#        return data

def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            #return redirect('password_change')
            return redirect('/')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'password_change_form.html', {
        'form': form
    })

def edit_user(request,user_pk):
    if request.method == "GET":
        pk = int(user_pk)
        if 0 < pk:
            srtuser = SrtUser.objects.get(pk=user_pk)
            if not UserSafe.is_admin(request.user):
                mode = 'edit_user'
            else:
                mode = 'edit_admin'
        else:
            # does this user have permission to create a new user?
            if not UserSafe.is_creator(request.user):
                return redirect('/')
            mode = 'new_admin'
        context = {
            'mode'       : mode,
            'user_name'  : '' if not pk else srtuser.username,
            'user_first' : '' if not pk else srtuser.first_name,
            'user_last'  : '' if not pk else srtuser.last_name,
            'user_email' : '' if not pk else srtuser.email,
            'user_role'  : '' if not pk else srtuser.role,
            'group_name' : 'Reader' if not pk else srtuser.get_groups.split(',')[0],
            'validation_errors' : '',
        }
        return render(request, 'user_edit.html', context)
    elif request.method == "POST":
        _log("edit_user:%s" % request.POST)
        if request.POST["action"] == "cancel":
            if UserSafe.is_admin(request.user):
                return redirect('users')
            else:
                return redirect('/')

        mode = request.POST.get('mode', '')
        user_name = request.POST.get('user_name', '')
        user_first = request.POST.get('user_first', '')
        user_last = request.POST.get('user_last', '')
        user_email = request.POST.get('user_email', '')
        user_role = request.POST.get('user_role', '')
        user_group = request.POST.get('user_group', '')
        user_pass1 = request.POST.get('user_pass1', '')
        user_pass2 = request.POST.get('user_pass2', '')

        # Validate
        validation_errors = ''
        if 'new_admin' == mode:
            try:
                srtuser = SrtUser.objects.get(username=user_name)
                validation_errors += ", Username is already registered"
            except Exception as e:
                _log("EDIT_USER:NAME_GET: error%s" % e)
            if not user_name:
                validation_errors += ", Missing user name"
            if not user_pass1 or not user_pass2:
                validation_errors += ", Missing password"
            if user_pass1 != user_pass2:
                validation_errors += ", Passwords do not match"
        else:
            if user_pass1 != user_pass2:
                validation_errors += ", Passwords do not match"
        if validation_errors:
            # Let them reuse their previous entries
            context = {
                'mode'       : mode,
                'user_name'  : user_name,
                'user_first' : user_first,
                'user_last'  : user_last,
                'user_email' : user_email,
                'user_role'  : user_role,
                'group_name' : user_group,
                'validation_errors' : validation_errors[2:],
            }
            return render(request, 'user_edit.html', context)

        # Process the post
        if 'new_admin' == mode:
            srtuser = SrtUser(username=user_name)
        else:
            if (UserSafe.user_name(request.user) != user_name) and (not UserSafe.is_admin(request.user)):
                _log("WARNING:EDIT_USERUSERNAME != SESSION USERNAME")
                return redirect('/')
            srtuser = SrtUser.objects.get(username=user_name)
        srtuser.first_name = user_first
        srtuser.last_name = user_last
        srtuser.email = user_email
        srtuser.role = user_role
        srtuser.save()
        # Update Group
        if user_group and (user_group != srtuser.get_groups.split(',')[0]):
            if not 'new_admin' == mode:
                for group_name in srtuser.get_groups.split(','):
                    group = Group.objects.get(name = group_name)
                    group.user_set.remove(srtuser)
            group = Group.objects.get(name = user_group)
            group.user_set.add(srtuser)
        # Force new password if sent
        if UserSafe.is_admin(request.user):
            if user_pass1 and (user_pass1 == user_pass2):
                srtuser.set_password(user_pass1)  # hash the password
                srtuser.save()
        # Done
        if UserSafe.is_admin(request.user):
            return redirect('users')
        else:
            return redirect('/')
    raise Exception("Invalid HTTP method for this page")


def xhr_user_commit(request):
    _log("xhr_user_commit(%s)" % request.POST)
    if not 'action' in request.POST:
        return HttpResponse(json.dumps({"error":"missing action\n"}), content_type = "application/json")

    action = request.POST['action']
    history_comment = ''
    try:
        if 'submit-trashuser' == action:
            record_id = request.POST['record_id']
            user = SrtUser.objects.get(pk=record_id).delete()
        return_data = {
            "error": "ok",
           }
        return HttpResponse(json.dumps( return_data ), content_type = "application/json")
    except Exception as e:
        _log("xhr_user_commit:no(%s)" % e)
        return HttpResponse(json.dumps({"error":str(e) + "\n" + traceback.format_exc()}), content_type = "application/json")

