from django.contrib.auth.models import AbstractUser, Group, AnonymousUser
from django.db import models
from django.contrib.auth.models import Group

# quick development/debugging support
from srtgui.api import _log

class SrtUser(AbstractUser):
    # add additional fields in here
    role = models.CharField(max_length=128, verbose_name='security role')

    def __str__(self):
        return "%s,%s" % (self.email,self.role)

    @property
    def is_guest(self):
        return self.username == "Guest"
    @property
    def is_reader(self):
        if self.is_superuser:
            return True
        return self.has_perm('users.Reader')
    @property
    def is_contributor(self):
        if self.is_superuser:
            return True
        return self.has_perm('users.Contributor')
    @property
    def is_creator(self):
        if self.is_superuser:
            return True
        return self.has_perm('users.Creator')
    @property
    def is_admin(self):
        if self.is_superuser:
            return True
        return self.has_perm('users.Admin')
    @property
    def user_name(self):
        return self.username
    @property
    def user_fullname(self):
        if self.first_name or self.last_name:
            fullname = '%s %s' % (self.first_name,self.last_name)
            return fullname.strip()
        else:
            return self.username
    @property
    def get_groups(self):
        groups = [ group.name for group in self.groups.all() ]
        if not groups:
            if self.is_superuser:
                return 'Superuser'
            else:
                return ''
        return ",".join(groups)
    @property
    def get_group_perm(self):
        return self.get_group_permissions()

# Minimal and safe User object to pass to web pages (no passwords)
class UserSafe():
    def __init__(self, pk, name, email, role):
        self.pk = pk
        self.name = name
        self.email = email
        self.role = role

    def __str__(self):
        return "UserSafeStr=%d,%s,%s" % (self.pk, self.name, self.email)

    @staticmethod
    def get_safe_userlist(allow_assignment=True):
        user_list = []
        for user in SrtUser.objects.all():
            if 'SRTool' == user.username:
                continue
            if allow_assignment and ('Guest' == user.username):
                continue
            u = UserSafe(user.id,user.username,user.email,user.role)
            user_list.append(u)
        return user_list

    @staticmethod
    def is_guest(user_obj):
        if isinstance(user_obj,AnonymousUser):
            return True
        return user_obj.username == "Guest"
    @staticmethod
    def is_reader(user_obj):
        if isinstance(user_obj,AnonymousUser):
            return False
        return user_obj.is_reader
    @staticmethod
    def is_contributor(user_obj):
        if isinstance(user_obj,AnonymousUser):
            return False
        return user_obj.is_contributor
    @staticmethod
    def is_creator(user_obj):
        if isinstance(user_obj,AnonymousUser):
            return False
        return user_obj.is_creator
    @staticmethod
    def is_admin(user_obj):
        if isinstance(user_obj,AnonymousUser):
            return False
        return user_obj.is_admin
    @staticmethod
    def user_name(user_obj):
        if isinstance(user_obj,AnonymousUser):
            return "Guest"
        return user_obj.username

