from django.conf.urls import include, url
from . import views

urlpatterns = [
    url(r'^signup/', views.SignUp.as_view(), name='signup'),
    url(r'^password_change/', views.change_password, name='password_change'),

    url(r'^password_change/', views.change_password, name='password_change'),
    url(r'^edit_user/(?P<user_pk>\d+)$', views.edit_user, name="edit_user"),

    url(r'^xhr_user_commit/$', views.xhr_user_commit, name='xhr_user_commit'),

]

