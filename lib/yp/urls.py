from django.conf.urls import include, url
from . import views

urlpatterns = [
    url(r'^hello/$', views.yp_hello, name='yp_hello'),

    url(r'^$', views.yp_hello, name='yp_default'),
]
