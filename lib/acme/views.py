#
# ex:ts=4:sw=4:sts=4:et
# -*- tab-width: 4; c-basic-offset: 4; indent-tabs-mode: nil -*-
#
# Security Response Tool Implementation
#
# Copyright (C) 2017-2018 Wind River Systems
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import json

from django.urls import reverse_lazy
from django.views import generic
from django.http import HttpResponse, HttpResponseNotFound, JsonResponse, HttpResponseRedirect

from django.contrib.auth.forms import UserCreationForm, UserChangeForm, PasswordChangeForm
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.models import Group
from django.shortcuts import render, redirect

from orm.models import SrtSetting, Product

from srtgui.views import MimeTypeFinder
from acme.reports import AcmeReportManager

# quick development/debugging support
from srtgui.api import _log

def acme_hello(request):
    context = {}
    return render(request, 'acme_hello.html', context)

def acme_product(request, product_pk):
    template = "acme_product.html"
    if Product.objects.filter(pk=product_pk).count() == 0 :
        return redirect('/')

    product_object = Product.objects.get(pk=product_pk)

    context = {
            'object'  : product_object,
    }
    return render(request, template, context)

def report(request,page_name):
    if request.method == "GET":
        context = AcmeReportManager.get_context_data(page_name,request=request)
        record_list = request.GET.get('record_list', '')
        _log("EXPORT_GET!:%s|%s|" % (request,record_list))
        context['record_list'] = record_list
        return render(request, 'report.html', context)
    elif request.method == "POST":
        _log("EXPORT_POST!:%s|%s" % (request,request.FILES))
        parent_page = request.POST.get('parent_page', '')
        file_name,response_file_name = AcmeReportManager.exec_report(parent_page,request=request)

        if file_name and response_file_name:
            fsock = open(file_name, "rb")
            content_type = MimeTypeFinder.get_mimetype(file_name)

            response = HttpResponse(fsock, content_type = content_type)

            disposition = "attachment; filename=" + response_file_name
            response["Content-Disposition"] = disposition

            _log("EXPORT_POST_Q{%s|" % (response))
            return response
        else:
            return render(request, "unavailable_artifact.html", {})

        return redirect('/')
    raise Exception("Invalid HTTP method for this page")

