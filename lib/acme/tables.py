#
# ex:ts=4:sw=4:sts=4:et
# -*- tab-width: 4; c-basic-offset: 4; indent-tabs-mode: nil -*-
#
# Security Response Tool Implementation
#
# Copyright (C) 2017       Wind River Systems
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from srtgui.widgets import ToasterTable
from orm.models import SrtSetting
from orm.models import Cve, Vulnerability, Investigation, CweTable, Product
from orm.models import Package, PackageToCve
from orm.models import CpeTable, CpeFilter, Defect, DataSource
from orm.models import PublishPending
from orm.models import Notify, NotifyAccess, NotifyCategories
from users.models import SrtUser, UserSafe

from django.db.models import Q, Max, Sum, Count, When, Case, Value, IntegerField
from django.conf.urls import url
from django.urls import reverse, resolve
from django.http import HttpResponse
from django.views.generic import TemplateView

from srtgui.tablefilter import TableFilter
from srtgui.tablefilter import TableFilterActionToggle
from srtgui.tablefilter import TableFilterActionDateRange
from srtgui.tablefilter import TableFilterActionDay

import os
import re

# quick development/debugging support
from srtgui.api import _log

class AcmeProductsTable(ToasterTable):
    """Table of All Products in SRTool"""

    def __init__(self, *args, **kwargs):
        super(AcmeProductsTable, self).__init__(*args, **kwargs)
        self.default_orderby = "order"

    def get_context_data(self, **kwargs):
        context = super(AcmeProductsTable, self).get_context_data(**kwargs)
        return context


    def setup_queryset(self, *args, **kwargs):
        self.queryset = \
                Product.objects.all()

        self.queryset = self.queryset.order_by(self.default_orderby)

    def setup_filters(self, *args, **kwargs):
        # Execution outcome types filter
        investigations = TableFilter(name="investigations",
                                       title="Filter Investigations by status")

        exec_investigation_action_open = TableFilterActionToggle(
            "open",
            "Open",
            Q(status=1))

        exec_investigation_action_open_high = TableFilterActionToggle(
            "not_executed",
            "Open High Priority",
            Q(status=1))

        investigations.add_action(exec_investigation_action_open)
        investigations.add_action(exec_investigation_action_open_high)
        self.add_filter(investigations)

    def setup_columns(self, *args, **kwargs):

        self.add_column(title="Order",
                        field_name="order",
                        hideable=False,
                        )

        key_link_template = '''
            <a href="{% url 'acmeproduct' data.id %}" id="dataid_{{data.id}}">{{data.key}}<a>
        '''
        self.add_column(title="ACME Key",
                        hideable=False,
                        orderable=True,
                        field_name="key",
                        static_data_name="key",
                        static_data_template=key_link_template,
                        )

        self.add_column(title="ACME Name",
                        field_name="name",
                        hideable=False,
                        )

        self.add_column(title="ACME Version",
                        field_name="version",
                        hideable=False,
                        )

        self.add_column(title="ACME Extension",
                        field_name="profile",
                        hideable=False,
                        )

        self.add_column(title="CPE",
                        field_name="cpe",
                        hideable=True,
                        hidden=True,
                        )

        self.add_column(title="Defect Tags",
                        field_name="defect_tags",
                        hideable=False,
                        )

        self.add_column(title="Product Tags",
                        field_name="product_tags",
                        hideable=False,
                        )


        investigations_link_template = '''
            {% if data.product_investigation.all.count %}
                <a href="{% url 'investigations' %}?filter=is_product:{{data.key}}&">
                    {{data.product_investigation.all.count}}
                </a>
            {% else %}0{% endif %}
        '''
        self.add_column(title="Investigations",
                    field_name="investigations",
                    hidden=False,
                    static_data_name="investigations",
                    static_data_template=investigations_link_template,
                    )

        defects_link_template = '''
            {% if data.product_defect.all.count %}
                <a href="{% url 'defects' %}?filter=is_product:{{data.key}}&">
                    {{data.product_defect.all.count}}
                </a>
            {% else %}0{% endif %}
        '''
        self.add_column(title="Defects",
                    field_name="defects",
                    hidden=False,
                    static_data_name="defects",
                    static_data_template=defects_link_template,
                    )


class AcmeDefectsTable(ToasterTable):
    """Table of All Defects in SRTool"""

    def __init__(self, *args, **kwargs):
        super(AcmeDefectsTable, self).__init__(*args, **kwargs)
        self.default_orderby = "name"

    def get_context_data(self, **kwargs):
        context = super(AcmeDefectsTable, self).get_context_data(**kwargs)
        return context


    def setup_filters(self, *args, **kwargs):
        # Priority filter
        is_priority = TableFilter(name="is_priority",
                                       title="Filter defects by 'Priority'")
        for priority in range(len(Defect.Priority)):
            is_priority.add_action(TableFilterActionToggle(
                Defect.Priority[priority][1].lower().replace(' ','_'),
                Defect.Priority[priority][1],
                Q(priority=Defect.Priority[priority][0]))
                )
        self.add_filter(is_priority)

        # Status filter
        is_status = TableFilter(name="is_status",
                                       title="Filter defects by 'Status'")
        for status in range(len(Defect.Status)):
            is_status.add_action(TableFilterActionToggle(
                Defect.Status[status][1].lower().replace(' ','_'),
                Defect.Status[status][1],
                Q(status=Defect.Status[status][0]))
                )
        self.add_filter(is_status)

        # Resolution filter
        is_resolution = TableFilter(name="is_resolution",
                                       title="Filter defects by 'Resolution'")
        for resolution in range(len(Defect.Resolution)):
            is_resolution.add_action(TableFilterActionToggle(
                Defect.Resolution[resolution][1].lower().replace(' ','_'),
                Defect.Resolution[resolution][1],
                Q(resolution=Defect.Resolution[resolution][0]))
                )
        self.add_filter(is_resolution)

        # Product filter
        #(name="Acme",version="Looney")
        is_product = TableFilter(name="is_product",
                                       title="Filter defects by 'Product'")
        for product in Product.objects.all():
            is_product.add_action( TableFilterActionToggle(
                product.key,
                product.long_name,
                Q(product=product))
                )
        self.add_filter(is_product)


    def setup_queryset(self, *args, **kwargs):
        self.queryset = \
                Defect.objects.all()

        self.queryset = self.queryset.order_by(self.default_orderby)

    def setup_columns(self, *args, **kwargs):

        name_link_template = '''
            <a href="{% url 'defect_name' data.name %}" id="dataid_{{data.id}}">{{data.name}}</a>
        '''
        self.add_column(title="Name",
                        hideable=False,
                        orderable=True,
                        field_name="name",
                        static_data_name="name",
                        static_data_template=name_link_template,
                        )

        self.add_column(title="ACME Summary",
                        field_name="summary",
                        )

        self.add_column(title="ACME Priority",
                        hideable=False,
                        field_name="priority",
                        orderable=True,
                        filter_name="is_priority",
                        static_data_name="priority",
                        static_data_template='{{data.get_priority_text}}',
                        )

        self.add_column(title="ACME Status",
                        hideable=False,
                        field_name="status",
                        orderable=True,
                        filter_name="is_status",
                        static_data_name="status",
                        static_data_template='{{data.get_status_text}}',
                        )

        self.add_column(title="ACME Resolution",
                        hideable=False,
                        field_name="resolution",
                        orderable=True,
                        filter_name="is_resolution",
                        static_data_name="resolution",
                        static_data_template='{{data.get_resolution_text}}',
                        )

        self.add_column(title="ACME Release Version",
                        hideable=True,
                        orderable=True,
                        field_name="release_version",
                        )

        self.add_column(title="ACME Publish",
                        hideable=True,
                        orderable=True,
                        field_name="publish",
                        )

        url_link_template = '''
            <a href="{{data.url}}" target="_blank">{{data.url}}</a>
        '''
        self.add_column(title="URL",
                        field_name="url",
                        hideable=True,
                        hidden=True,
                        static_data_name="url",
                        static_data_template=url_link_template,
                        )

        investigations_link_template = '''
            {% for di in data.defect_to_investigation.all %}
              {% if not forloop.first %} {% endif %}<a href="{% url 'investigation' di.investigation.name %}" target="_blank">{{di.investigation.name}} </a>
            {% endfor %}
        '''
        self.add_column(title="Investigation",
                        hideable=True,
                        static_data_name="investigation",
                        static_data_template=investigations_link_template,
                        )

        # !!! HACK: 'data.product' is returning '%s' when it is supposed to be null !!!
        product_link_template = '''
            {% if data.product != '%s' %}
              <a href="{% url 'product' data.product.id %}">
                {{data.product.long_name}}
              </a>
            {% endif %}
        '''
        self.add_column(title="Product",
                        hideable=True,
                        orderable=True,
                        filter_name="is_product",
                        static_data_name="product",
                        static_data_template=product_link_template,
                        )

        self.add_column(title="SRT Update",
                        hideable=True,
                        hidden=True,
                        orderable=True,
                        field_name="srt_updated",
                        static_data_name="srt_updated",
                        static_data_template='{{data.srt_updated | date:"m/d/y H:i"}}'
                        )

