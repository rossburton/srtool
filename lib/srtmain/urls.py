#
# ex:ts=4:sw=4:sts=4:et
# -*- tab-width: 4; c-basic-offset: 4; indent-tabs-mode: nil -*-
#
# Security Response Tool Implementation
#
# Copyright (C) 2016       Wind River Systems
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import os

from django import VERSION as DJANGO_VERSION
from django.conf.urls import url
from django.views.generic import RedirectView, TemplateView
from django.views.decorators.cache import never_cache

if DJANGO_VERSION >= (2,0):
    from django.urls import path, include  #pylint: disable=no-name-in-module
else:
    from django.conf.urls import include  #pylint: disable=no-name-in-module

import logging

logger = logging.getLogger("srt")

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = [

    # Examples:
    # url(r'^srt/', include('srt.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    #url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    url(r'^health$', TemplateView.as_view(template_name="health.html"), name='Toaster Health'),

    # if no application is selected, we have the magic srtgui app here
    url(r'^$', never_cache(RedirectView.as_view(url='/srtgui/', permanent=True))),
]

import srtmain.settings

if srtmain.settings.FRESH_ENABLED:
    urlpatterns.insert(1, url(r'', include('fresh.urls')))
    #logger.info("Enabled django-fresh extension")

if srtmain.settings.DEBUG_PANEL_ENABLED:
    try:
        import debug_toolbar
        urlpatterns.insert(1, url(r'', include(debug_toolbar.urls)))
        #logger.info("Enabled django_toolbar extension")
    except ImportError:
        logger.warn("Debug panel enabled but django-debug-toolbar not found")

# Fetch the main app
SRT_MAIN_APP = os.environ.get('SRT_MAIN_APP', 'yp')

# Django user model
if DJANGO_VERSION >= (2,0):
    urlpatterns = [
        # Uncomment the next lines to enable the admin:
        path('admin/', admin.site.urls),

        # Main application
        path(SRT_MAIN_APP + '/', include(SRT_MAIN_APP + '.urls')),
        # Default applications
        path('srtgui/', include('srtgui.urls')),
        path('users/', include('users.urls')),
        path('users/', include('django.contrib.auth.urls')),
    ] + urlpatterns
else:
    urlpatterns = [
        # Uncomment the next lines to enable the admin:
        url(r'^admin/', include(admin.site.urls)),

        # Default applications
        url(r'^users/', include('users.urls')),
        url(r'^users/', include('django.contrib.auth.urls')),
        url(r'^srtgui/', include('srtgui.urls')),
        # Main application
        url('^' + SRT_MAIN_APP + '/', include(SRT_MAIN_APP + '.urls')),

    ] + urlpatterns
#print("DEBUG:INSTALLED_URL_PATTERNS:%s,%s" % (SRT_MAIN_APP,urlpatterns))

currentdir = os.path.dirname(__file__)

from pprint import pformat
#logger.debug("urlpatterns list %s", pformat(urlpatterns))

is_debug = False
if is_debug:
    print("DEBUG:urlpatterns list %s", pformat(urlpatterns))
    from django.apps import apps
    for app in apps.get_app_configs():
        print("DEBUG:%s:" % app.verbose_name)
        for model in app.get_models():
            print("\t", model)
