from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ObjectDoesNotExist
from django.db import OperationalError
import os

class Command(BaseCommand):
    help = "Trigger a data source update"

    def add_arguments(self, parser):
        parser.add_argument('--cron-start', action='store_const', const='cron_start', dest='command', help='Start the SRTool backgroud updater')
        parser.add_argument('--cron-stop', action='store_const', const='cron_stop', dest='command', help='Stop the SRTool backgroud updater')

        parser.add_argument('--list', '-l', action='store_const', const='list', dest='command', help='List data sources')
        parser.add_argument('--run-updates', '-u', action='store_const', const='run-updates', dest='command', help='update scheduled data sources')
        parser.add_argument('--force', '-f', action='store_true', dest='force', help='Force the update')
        parser.add_argument('--name-filter', '-n', nargs='+', type=str, dest='name_filter', help='Filter for datasource name')

        parser.add_argument('--verbose', action='store_true', dest='verbose', help='Debugging: verbose output')
        parser.add_argument('--trial', '-t', action='store_true', dest='is_trial', help='Debugging: trial run')

    def handle(self, *args, **options):
        #print("UPDATE:%s|%s" % (str(args),str(options)))

        command = ''
        if 'cron_start' == options['command']: command = '--cron-start'
        if 'cron_stop' == options['command']: command = '--cron-stop'
        if 'list' == options['command']: command = '--list'
        if 'run-updates' == options['command']: command = '--run-updates'

        # NOTE: we have to do shenanigans with name_filter to support spaces
        name_filter = '--name-filter "%s"' % ' '.join(options['name_filter']) if options['name_filter'] else ''

        force = '--force' if options['force'] else ''
        is_trial = '--trial' if options['is_trial'] else ''
        verbose = '--verbose' if options['verbose'] or (options['verbosity'] > 1) else ''
        context = '> /dev/null 2>&1 &' if 'cron_start' == options['command'] else ''

        update_command = "./bin/common/srtool_update.py %s %s %s %s %s %s" % (command,name_filter,force,is_trial,verbose,context)
        if verbose:
            print("RUN UPDATE SCRIPT: %s" % (update_command))
        os.chdir(os.environ['SRT_BASE_DIR'])
        os.system("%s" % (update_command))
